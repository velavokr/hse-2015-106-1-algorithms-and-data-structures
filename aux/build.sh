#!/usr/bin/env bash
set -ue

me=$0
project=${1:-"--help"}
opts=${2:-"-O2"}

if [ "--help" == "$project" ]; then
    echo "$me <project> ["<gcc opts>"]"
    exit 0
fi

cd ${project}

g++ -std=c++11 -Wall ${opts} -o "${project}" "${project}.cpp"

fail=0
for input in input.*.txt; do
    "./${project}" < "${input}" > "out.${input}"
    
    if [ $? -ne 0 ]; then
        echo "RUN ${input} FAILED"
        exit 1
    fi
    
    /usr/bin/diff -du "ref.${input}" "out.${input}" && echo "TEST ${input} OK"
    if [ $? -ne 0 ]; then
        echo "TEST ${input} FAILED"
        fail=$(( fail + 1 ))
    fi
done

if [[ $fail -ne 0 ]]; then
    echo "${fail} TESTS FAILED"
else
    echo "SUCCESS"
fi
