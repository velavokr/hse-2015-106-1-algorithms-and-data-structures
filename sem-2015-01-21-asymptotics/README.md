[en:wiki "Big O" notation](https://en.wikipedia.org/wiki/Big_O_notation)

[ru:wiki "О большое" - нотация](https://ru.wikipedia.org/wiki/%C2%ABO%C2%BB_%D0%B1%D0%BE%D0%BB%D1%8C%D1%88%D0%BE%D0%B5_%D0%B8_%C2%ABo%C2%BB_%D0%BC%D0%B0%D0%BB%D0%BE%D0%B5#.D0.94.D1.80.D1.83.D0.B3.D0.B8.D0.B5_.D0.BF.D0.BE.D0.B4.D0.BE.D0.B1.D0.BD.D1.8B.D0.B5_.D0.BE.D0.B1.D0.BE.D0.B7.D0.BD.D0.B0.D1.87.D0.B5.D0.BD.D0.B8.D1.8F)

[en:wiki The master theorem](http://en.wikipedia.org/wiki/Master_theorem)

[ru:wiki Основная теорема о рекуррентных соотношениях](https://ru.wikipedia.org/wiki/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D0%BD%D0%B0%D1%8F_%D1%82%D0%B5%D0%BE%D1%80%D0%B5%D0%BC%D0%B0_%D0%BE_%D1%80%D0%B5%D0%BA%D1%83%D1%80%D1%80%D0%B5%D0%BD%D1%82%D0%BD%D1%8B%D1%85_%D1%81%D0%BE%D0%BE%D1%82%D0%BD%D0%BE%D1%88%D0%B5%D0%BD%D0%B8%D1%8F%D1%85)

[en:wiki The Akra-Bazzi method](http://en.wikipedia.org/wiki/Akra%E2%80%93Bazzi_method)