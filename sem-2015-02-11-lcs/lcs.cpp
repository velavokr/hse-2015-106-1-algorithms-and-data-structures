#include "lcs.h"

#include <map>
#include <sstream>

struct parameters {
    unsigned string_a_size;
    unsigned string_b_size;
    unsigned seed;

    parameters()
        : string_a_size(15)
        , string_b_size(15)
        , seed(1)
    {}
};


string generate_random_string(size_t size) {
    string s;
    s.resize(size);

    for (size_t i = 0; i < size; ++i) {
        if (i % 3) {
            const char vowels[] = {
                            'a', 'e', 'i', 'o', 'u', 'y'
            };

            s[i] = vowels[rand() % sizeof(vowels)];
        } else {
            const char consonants[] = {
                            'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
                            'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'
            };

            s[i] = consonants[rand() % sizeof(consonants)];
        }
    }

    return s;
}


void align_strings(string_vec& result, char c) {
    size_t max_sz = 0;
    for (string_vec::iterator it = result.begin(); it != result.end(); ++it) {
        max_sz = max(max_sz, it->size());
    }

    for (string_vec::iterator it = result.begin(); it != result.end(); ++it) {
        it->resize(max_sz, c);
    }
}


string_vec diff_strings(const string& a, const string& lcs, const string& b) {
    string_vec result;
    result.resize(3);

    size_t a_i = 0;
    size_t b_i = 0;
    const size_t a_sz = a.size();
    const size_t b_sz = b.size();

    for (size_t lcs_i = 0, lcs_sz = lcs.size(); lcs_i < lcs_sz; ++lcs_i) {
        while (a_i < a_sz && a[a_i] != lcs[lcs_i]) {
            result[0].push_back(a[a_i]);
            ++a_i;
        }

        while (b_i < b_sz && b[b_i] != lcs[lcs_i]) {
            result[2].push_back(b[b_i]);
            ++b_i;
        }

        align_strings(result, '_');

        result[1].push_back(lcs[lcs_i]);

        if (a_i < a_sz) {
            result[0].push_back(lcs[lcs_i]);
        }

        if (b_i < b_sz) {
            result[2].push_back(lcs[lcs_i]);
        }

        ++a_i;
        ++b_i;
    }

    result[0].insert(result[0].end(), a.begin() + a_i, a.end());
    result[2].insert(result[2].end(), b.begin() + b_i, b.end());

    align_strings(result, '_');

    return result;
}


string_vec split(const string& s, char delim) {
    string_vec elems;
    stringstream ss(s);
    string item;

    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }

    if (elems.empty())
        elems.push_back(s);

    return elems;
}


bool find_in_string(const string& lcs, const string& a, const string& label) {
    size_t pos = 0;
    for (string::const_iterator it = lcs.begin(); it != lcs.end(); ++it) {
        size_t p = a.find(*it, pos);

        if (string::npos == p) {
            cout << "error: string " << label << " does not contain '" << *it << "' from lcs" << endl;
            return false;
        }

        pos = max(p, pos);
    }

    return true;
}


size_t measure_lcs(const string& a, const string& b) {
    /*
    Комбинаторный алгоритм LCS через LIS, сложность O(nm/s), s - размер алфавита.
    Эффективен при больших алфавитах s = O(n) (таковой можно получить, например,
    захэшировав слова в текстах). Если необходима только длина LCS, сама LCS строиться не будет.
    См. Gusfield, Algorithms on Strings, Trees and Sequences, 1997, Chapt.12.5
    Или здесь: http://www.cs.ucf.edu/courses/cap5937/fall2004/Longest%20common%20subsequence.pdf

    Краткое изложение идеи:
        Пусть мы имеем последовательность чисел.

        Обозначим:
            IS - подпоследовательность, строго возрастающую слева направо.
            LIS - наибольшая IS исходной последовательности.
            DS - подпоследовательность, не убывающая слева направо.
            C - покрытие непересекающимися DS исходной последовательности.
            SC - наименьшее такое покрытие.

        Несложно доказывается теорема о том, что размеры SC и LIS совпадают, и из SC можно построить LIS.

        Далее, пусть у нас есть 2 строки S1 и S2. Можно показать, что если для каждого символа из S1
        взять список его вхожений в S2 в обратном порядке, и все такие списки по порядку сконкатенировать,
        то любая IS в полученном списке будет эквивалентна какой-либо общей подпоследовательности S1 и S2
        такой же длины. И, следовательно, LIS будет эквивалентна LCS.

        Идея алгоритма построения DS:
            Идя вдоль исходной последовательности, для текущего её члена x в списке её DS находим
            самую левую такую, что её последний член не меньше x. Если такая есть, добавляем x в конец.
            Если нет, добавляем в список DS новую DS, состоящую из x.

        Можно показать, что так построенный список DS будет SC.
     */
    typedef vector<size_t> subseq;
    typedef map<char, vector<size_t> > index;
    index idx;

    for (string::const_iterator it = a.begin(); it != a.end(); ++it)
        idx[*it];

    for (string::const_iterator it = b.begin(); it != b.end(); ++it) {
        index::iterator hit = idx.find(*it);
        if (hit != idx.end())
            hit->second.push_back(it - b.begin());
    }

    subseq lastindex;
    lastindex.reserve(max(a.size(), b.size()));

    for (string::const_iterator it = a.begin(); it != a.end(); ++it) {
        const subseq& sub2 = idx[*it];

        for (subseq::const_reverse_iterator it2 = sub2.rbegin(); it2 != sub2.rend(); ++it2) {
            size_t x = *it2;
            subseq::iterator lit = lower_bound(lastindex.begin(), lastindex.end(), x);

            if (lit == lastindex.end()) {
                lastindex.push_back(x);
            } else {
                *lit = x;
            }
        }
    }

    return lastindex.size();
}


void test_and_print_lcs(const string& a, const string& lcs, const string& b) {
    string_vec res = diff_strings(a, lcs, b);

    bool ok = true;
    for (string_vec::const_iterator it = res.begin(); it != res.end(); ++it) {
        cout << *it << endl;
    }

    cout << endl;

    if (lcs.size() > min(a.size(), b.size())) {
        cout << "error: lcs bigger than one of strings (" << lcs.size() << " > " << min(a.size(), b.size()) << endl;
        ok = false;
    }

    ok = ok && find_in_string(lcs, a, "a");
    ok = ok && find_in_string(lcs, b, "b");

    if (lcs.empty()) {
        for (string::const_iterator it = a.begin(); it != a.end(); ++it) {
            if (string::npos != b.find(*it)) {
                cout << "error: lcs is empty while there is an intersection between strings" << endl;
                ok = false;
                break;
            }
        }
    }

    size_t lcs_sz = measure_lcs(a, b);
    if (lcs.size() != lcs_sz) {
        cout << "error: lcs is not equal to the one magically computed (" << lcs.size() << " != " << lcs_sz << ")" << endl;
        ok = false;
    }

    if (ok) {
        cout << endl << "tests OK" << endl;
    } else {
        cout << endl << "tests failed" << endl;
    }
}


void run_assignments(const string& a, const string& b) {
    cout << "=================================" << endl << endl;
    cout << "string a: " << a << endl;
    cout << "string b: " << b << endl;
    cout << endl;

    string lcs_top_down = find_lcs_top_down(a, b);
    cout << "lcs top down (" << lcs_top_down.size() << "): " << lcs_top_down << endl;
    test_and_print_lcs(a, lcs_top_down, b);
    cout << endl;

    string lcs_bottom_up = find_lcs_bottom_up(a, b);
    cout << "lcs bottom up (" << lcs_bottom_up.size() << "): " << lcs_bottom_up << endl;
    test_and_print_lcs(a, lcs_bottom_up, b);
    cout << endl;

    if (lcs_top_down.size() != lcs_bottom_up.size()) {
        cout << "error: top-down and bottom-up results differ ("
                        << lcs_top_down.size() << " != " << lcs_bottom_up.size() << ")" << endl;
    }
}


void print_help_and_exit(const char* me, const parameters& params) {
    cout << "Usage:" << endl;
    cout << "1. Specifying the items externally: "
                    << me << " -S string_a/string_b" << endl;
    cout << endl;
    cout << "2. Generating random sets of items: "
                    << me << " -R [string_a_size,string_b_size[:random_seed]]" << endl;
    cout << "   defaults: string_a_size = " << params.string_a_size << endl;
    cout << "             string_b_size = " << params.string_b_size << endl;
    cout << "             random_seed = " << params.seed << endl;
    cout << endl;
    exit(0);
}


void print_arg_error_and_exit(const string& arg, const string& arg_val, const string& what) {
    cout << "error in arg " << arg << ": expected " << what << ", got '" << arg_val << "'" << endl;
    cout << "see --help for options" << endl;
    exit(1);
}


void print_arg_error_and_exit(const string& arg, const string& arg_val) {
    if ("-S" == arg) {
        print_arg_error_and_exit(arg, arg_val,
            "max_weight:item_weight,item_value/item_weight,item_value/... All weights are > 0");
    } else if ("-R" == arg) {
        print_arg_error_and_exit( arg, arg_val,
            "max_knapsack_weight,number_of_items,[max_item_weight,max_item_value][:random_seed]... All numbers are > 0");
    }

    print_arg_error_and_exit("unknown error (bug?)", arg_val);
}


int parse_int_token(const string& arg, const string& arg_value,
                    const string_vec& tokens, size_t off, int deflt, bool required = false)
{
    if (off < tokens.size()) {
        int result = atoi(tokens[off].c_str());

        if (!result && "0" != tokens[off]) {
            print_arg_error_and_exit(arg, arg_value);
        }

        return result;
    } else if (required) {
        print_arg_error_and_exit(arg, arg_value);
    }

    return deflt;
}


unsigned parse_unsigned_token(const string& arg, const string& arg_value,
                              const string_vec& tokens, size_t off, unsigned deflt, bool required = false)
{
    int result = parse_int_token(arg, arg_value, tokens, off, deflt, required);

    if (result <= 0) {
        print_arg_error_and_exit(arg, arg_value);
    }

    return result;
}


int parse_required_int_token(const string& arg, const string& arg_value, const string_vec& tokens, size_t off) {
    return parse_int_token(arg, arg_value, tokens, off, 0, true);
}


unsigned parse_required_unsigned_token(const string& arg, const string& arg_value, const string_vec& tokens, size_t off) {
    return parse_unsigned_token(arg, arg_value, tokens, off, 0, true);
}


void process_manual_input(const string& arg, const string& arg_value) {
    string_vec strings = split(arg_value, '/');

    if (strings.size() != 2) {
        print_arg_error_and_exit(arg, arg_value);
    }

    run_assignments(strings[0], strings[1]);
}


void process_random_generated_input(parameters params, const string& arg, const string& arg_value) {
    // max_knapsack_weight,number_of_items,[max_item_weight,max_item_value][:random_seed]
    if (!arg_value.empty()) {
        string_vec strings_and_seed = split(arg_value, ':');

        params.seed = parse_unsigned_token(arg, arg_value, strings_and_seed, 1, params.seed);

        string_vec params_raw = split(strings_and_seed[0], ',');

        params.string_a_size = parse_unsigned_token(arg, arg_value, params_raw, 0, params.string_a_size);
        params.string_b_size = parse_unsigned_token(arg, arg_value, params_raw, 1, params.string_b_size);
    }

    srand(params.seed);

    for (unsigned i = 0; i < params.string_a_size; ++i) {
        for (unsigned j = 0; j < params.string_b_size; ++j) {
            run_assignments(generate_random_string(i), generate_random_string(j));
        }
    }
}


int main(int argc, const char** argv) {
    parameters params;

    if (argc < 2) {
        print_help_and_exit(argv[0], params);
    }

    for (int i = 1; i < argc; ++i) {
        string arg(argv[i]);

        if ("--help" == arg || "-h" == arg || "-?" == arg) {
            print_help_and_exit(argv[0], params);
        }

        if ("-S" == arg) {
            i += 1;
            string arg_value(i < argc ? argv[i] : "");
            process_manual_input(arg, arg_value);
        } else if ("-R" == arg) {
            string arg_value(i + 1 < argc ? argv[i + 1] : "");

            if (!arg_value.empty() && arg_value[0] != '-') {
                i += 1;
            }

            process_random_generated_input(params, arg, arg_value);
        }
    }
}
