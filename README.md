# Материалы семинаров по алгоритмам и структурам данных группы 106-1 ВШЭ (2015)

## Оглавление семинаров
bitbucket так и не научился вставлять относительные ссылки в корневой README.md, так что оглавление только такое: 

**[оглавление](https://bitbucket.org/velavokr/hse-2015-106-1-algorithms-and-data-structures/src)**

## Полезные лекции MIT 6-006 Introduction To Algorithms
[Конспекты](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-spring-2008/lecture-notes/)

[Видео](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/)

## Курс The Hardware-Software Interface
[Видео](https://www.coursera.org/course/hwswinterface)

## Хорошие слайды по курсу (Algorithm Design by Jon Kleinberg and Eva Tardos, Princeton)
### Разделяй и властвуй
[Часть I](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/05DivideAndConquerI.pdf)

  * mergesort ([демо merge](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/05DemoMerge.pdf))
  * counting inversions ([демо merge-with-count](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/05DemoMergeInvert.pdf))
  * closest pair of points
  * randomized quicksort
  * median and selection ([демо quick-select](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/05DemoQuickSelect.pdf))
  
[Часть II](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/05DivideAndConquerII.pdf)

  * master theorem
  * integer multiplication
  * matrix multiplication
  * convolution and FFT
  
### Графы, жадные алгоритмы и динамическое программирование
[Графы](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/03Graphs.pdf)

  * basic definitions and applications
  * graph connectivity and graph traversal
  * testing bipartiteness
  * connectivity in directed graphs
  * DAGs and topological ordering

[Потоки I](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/07NetworkFlowI.pdf)

  * max-flow and min-cut problems
  * Ford-Fulkerson algorithm
  * max-flow min-cut theorem
  * capacity-scaling algorithm
  * shortest augmenting paths
  * blocking-flow algorithm
  * unit-capacity simple networks

[Потоки II](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/07NetworkFlowII.pdf)

  * bipartite matching
  * disjoint paths
  * extensions to max flow
  * survey design
  * airline scheduling
  * image segmentation
  * project selection
  * baseball elimination

[Потоки III](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/07NetworkFlowIII.pdf)

  * assignment problem
  * input-queued switching

[A*](http://www.cs.cmu.edu/~./awm/tutorials/astar.html)

[Жадные алгоритмы I](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/04GreedyAlgorithmsI.pdf)

  * coin changing
  * interval scheduling ([демо earliest finish first](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/04DemoEarliestFinishTimeFirst.pdf), [демо earliest start first](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/04DemoEarliestStartTimeFirst.pdf))
  * scheduling to minimize lateness
  * optimal caching

[Жадные алгоритмы II](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/04GreedyAlgorithmsII.pdf)

  * Dijkstra's algorithm ([демо Dijkstra](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/04DemoDijkstra.pdf))
  * minimum spanning trees
  * Prim, Kruskal, Boruvka ([демо Prim](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/04DemoPrim.pdf), [демо Kruskal](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/04DemoKruskal.pdf), [демо Boruvka](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/04DemoBoruvka.pdf))
  * single-link clustering
  * min-cost arborescences

[Динамическое программирование I](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/06DynamicProgrammingI.pdf) 

  * weighted interval scheduling
  * segmented least squares
  * knapsack problem
  * RNA secondary structure
  
[Динамическое программирование II](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/06DynamicProgrammingII.pdf)

  * sequence alignment ([демо LIS](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/LongestIncreasingSubsequence.pdf))
  * Hirschberg's algorithm
  * Bellman-Ford algorithm
  * distance vector protocols
  * negative cycles in a digraph

## Полезные ссылки:
[Справка по С++ (язык и stl)](http://en.cppreference.com/w/)

[Справка по С++ (язык и stl) на русском](http://ru.cppreference.com/w/)

[Справка по C++ (stl)](http://www.cplusplus.com/)

[Очень удобный онлайн-редактор TeX с подробной справкой и примерами](https://ru.sharelatex.com/)

## Книги по курсу:
### На русском
[Алгоритмы (С.Дасгупта, Х.Пападимитриу, У.Вазирани)](https://yadi.sk/i/LIRcfLmke58Fs)

[Алгоритмы, построение и анализ, 3-е издание (Кормен, Лейзерсон, Ривест, Стейн)](https://yadi.sk/d/LKXrcRaGe8uz4)
### На английском
[Introduction to Algorithms 3rd edition (Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, Clifford Stein)](https://yadi.sk/i/mMs5YYCue58Qv)

[Algorithm Design (Jon Kleinberg, Eva Tardos)](https://yadi.sk/d/RPNdd6z4eNWcX)

[Mathematics for Computer Science (Eric Lehman, F. Thomson Leighton, Albert R. Meyer)](https://www.cs.princeton.edu/courses/archive/fall13/cos340/LLM.pdf)