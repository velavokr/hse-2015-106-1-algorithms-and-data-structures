Задача про расписание с весами.

Алгоритм см. в [слайдах](http://www.cs.princeton.edu/~wayne/kleinberg-tardos/pdf/06DynamicProgrammingI.pdf) и лекции.

Заготовка для задачи [тут](knapsack.h).

Тестовый фреймворк [тут](knapsack.cpp).
