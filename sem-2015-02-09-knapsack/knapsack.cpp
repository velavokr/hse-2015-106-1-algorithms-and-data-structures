#include "knapsack.h"

using namespace knapsack_task;
using namespace std;

// test stuff

struct parameters {
    unsigned knapsack_weight_max;
    unsigned num_items;
    unsigned item_weight_max;
    unsigned item_value_max;
    unsigned seed;

    parameters()
        : knapsack_weight_max(20)
        , num_items(20)
        , item_weight_max(10)
        , item_value_max(100)
        , seed(1)
    {}
};


item_vec generate_random_items(const parameters& params, bool reset_seed) {
    if (reset_seed)
        srand(params.seed);

    item_vec items;

    for (unsigned i = 0; i < params.num_items; ++i) {
        unsigned w = 1 + (unsigned(rand()) % params.item_weight_max);
        int v = rand() % (2 * params.item_value_max) - params.item_value_max;
        items.push_back(item(i + 1, w, v));
    }

    return items;
}


void print_knapsack(const knapsack& sack) {
    streamsize width = log10(max<double>(sack.items.size(), 1)) + 1;
    streamsize oldwidth = cout.width();

    unsigned total_weight = 0;
    int total_value = 0;
    cout << string(width + 1, ' ') << string(sack.max_total_weight, '~') << '\\' << endl;

    for (size_t i = 0, sz = sack.items.size(); i < sz; ++i) {
        item t = sack.items[i];

        cout.width(width);
        cout << i;
        cout.width(oldwidth);
        cout << ' ';

        cout << string(total_weight, ' ') << string(t.weight, '#') << ' ' << t.to_string() << endl;
        total_weight += t.weight;
        total_value += t.value;
    }

    cout << string(width + 1, ' ') << string(sack.max_total_weight, '~') << '/' << endl;
    cout << "num_items:" << sack.items.size()
                    << ", max_weight: " << sack.max_total_weight
                    << ", actual_weight: " << total_weight
                    << ", value: " << total_value << endl;
}


item find_small_max_item(const item_vec& items, unsigned max_weight) {
    item result;

    for (item_vec::const_iterator it = items.begin(); it != items.end(); ++it) {
        if (it->weight <= max_weight && it->value > result.value)
            result = *it;
    }

    return result;
}


item find_smallest_good_item(const item_vec& items, unsigned max_weight) {
    item result(0, max_weight, 0);

    for (item_vec::const_iterator it = items.begin(); it != items.end(); ++it) {
        if (it->weight <= max_weight && it->value > 0 && it->weight < result.weight) {
            result = *it;
        }
    }

    return result;
}


unsigned calculate_total_weight(item_vec::const_iterator begin, item_vec::const_iterator end) {
    unsigned result = 0;

    for (item_vec::const_iterator it = begin; it != end; ++it) {
        result += it->weight;
    }

    return result;
}


unsigned calculate_total_weight(const item_vec& items) {
    return calculate_total_weight(items.begin(), items.end());
}


int calculate_total_value(item_vec::const_iterator begin, item_vec::const_iterator end) {
    int result = 0;
    for (item_vec::const_iterator it = begin; it != end; ++it) {
        result += it->value;
    }

    return result;
}


int calculate_total_value(const item_vec& items) {
    return calculate_total_value(items.begin(), items.end());
}


item_vec find_what_is_left(const knapsack& sack, const item_vec& source_items) {
    set<item> sack_items(sack.items.begin(), sack.items.end());

    item_vec result;

    for (item_vec::const_iterator it = source_items.begin(); it != source_items.end(); ++it) {
        if (sack_items.find(*it) != sack_items.end()) {
            continue;
        }

        result.push_back(*it);
    }

    return result;
}


item_vec find_duplicates(const knapsack& sack) {
    set<item> sack_items;
    item_vec result;

    for (item_vec::const_iterator it = sack.items.begin(); it != sack.items.end(); ++it) {
        if (sack_items.find(*it) != sack_items.end()) {
            result.push_back(*it);
        } else {
            sack_items.insert(*it);
        }
    }

    return result;
}


item find_bad_item(const item_vec& items) {
    for (item_vec::const_iterator it = items.begin(); it != items.end(); ++it) {
        if (it->value <= 0)
            return *it;
    }

    return item();
}


bool gt_by_weight(const item& a, const item& b) {
    return a.weight > b.weight;
}


bool lt_by_weight(const item& a, const item& b) {
    return a.weight < b.weight;
}


bool gt_by_value(const item& a, const item& b) {
    return a.value > b.value;
}


bool lt_by_value(const item& a, const item& b) {
    return a.value < b.value;
}

typedef pair<item_vec, item_vec> items_for_items_replace;

items_for_items_replace do_find_better_items(const item_vec& sack_items, const item_vec& left_items, unsigned max_weight) {
    unsigned weight = 0;
    int value = 0;
    for (item_vec::const_iterator sit = sack_items.begin(); sit != sack_items.end(); ++sit) {
        weight += sit->weight;
        value += sit->value;

        int possible_value = 0;
        unsigned possible_weight = 0;
        for (item_vec::const_iterator lit = left_items.begin(); lit != left_items.end(); ++lit) {
            if (lit->value > 0 && lit->weight < max_weight) {
                possible_value += lit->value;
                possible_weight += lit->weight;
            }

            if (possible_value > value && possible_weight <= weight) {
                return items_for_items_replace(item_vec(sack_items.begin(), sit + 1),
                                               item_vec(left_items.begin(), lit + 1));
            }
        }
    }

    return items_for_items_replace();
}

items_for_items_replace find_better_items(item_vec sack_items, item_vec left_items, unsigned max_weight) {
    stable_sort(sack_items.begin(), sack_items.end(), gt_by_weight);

    // 1. try to replace the most heavy by the most valuable
    stable_sort(left_items.begin(), left_items.end(), gt_by_value);

    {
        items_for_items_replace replace = do_find_better_items(sack_items, left_items, max_weight);

        if (!replace.first.empty() && !replace.second.empty()) {
            return replace;
        }
    }

    // 2. try to replace the most heavy by the least heavy
    stable_sort(left_items.begin(), left_items.end(), lt_by_weight);

    {
        items_for_items_replace replace = do_find_better_items(sack_items, left_items, max_weight);

        if (!replace.first.empty() && !replace.second.empty()) {
            return replace;
        }
    }

    stable_sort(sack_items.begin(), sack_items.end(), lt_by_value);

    // 3. try to replace the least valuable by the most valuable
    stable_sort(left_items.begin(), left_items.end(), gt_by_value);

    {
        items_for_items_replace replace = do_find_better_items(sack_items, left_items, max_weight);

        if (!replace.first.empty() && !replace.second.empty()) {
            return replace;
        }
    }
    // 4. try to replace the least valuable by the least heavy
    stable_sort(left_items.begin(), left_items.end(), lt_by_weight);

    {
        items_for_items_replace replace = do_find_better_items(sack_items, left_items, max_weight);

        if (!replace.first.empty() && !replace.second.empty()) {
            return replace;
        }
    }

    return items_for_items_replace();
}


void print_items(item_vec items) {
    sort(items.begin(), items.end());

    for (item_vec::const_iterator it = items.begin(); it != items.end(); ++it) {
        if (it != items.begin()) {
            cout << ", ";
        }

        cout << it->to_string();
    }
}


bool test_knapsack(const knapsack& sack, const item_vec& source_items) {
    bool ok = true;
    item_vec left_items = find_what_is_left(sack, source_items);

    item min_item = find_smallest_good_item(left_items, sack.max_total_weight);
    items_for_items_replace replace = find_better_items(sack.items, left_items, sack.max_total_weight);

    unsigned weight = calculate_total_weight(sack.items);
    unsigned weight_left = max(sack.max_total_weight, weight) - weight;

    item_vec dups = find_duplicates(sack);

    // no more weight than max
    if (weight > sack.max_total_weight) {
        cout << "error: the knapsack has more weight than allowed ("
                        << weight << " > " << sack.max_total_weight << ")" << endl;
        ok = false;
    }

    // no bad items should be included
    if (item bad_item = find_bad_item(sack.items)) {
        cout << "error: the knapsack has a bad item (" << bad_item.to_string() << ")" << endl;
        ok =false;
    }

    // the result cannot be improved easily
    if (!replace.first.empty() && !replace.second.empty()) {
        cout << "error: the knapsack could be optimized by replacing some items (";
        print_items(replace.first);
        cout << " for ";
        print_items(replace.second);
        cout << ")" << endl;
        ok = false;
    }

    // the sack has no duplicates
    if (!dups.empty()) {
        cout << "error: the knapsack has duplicates of items: ";
        for (item_vec::const_iterator it = dups.begin(); it != dups.end(); ++it) {
            if (it != dups.begin()) {
                cout << ",";
            }

            cout << it->to_string();
        }

        ok = false;
    }

    // the sack is not empty if there is something valuable and small enough to put in it
    if (sack.items.empty()) {
        if (item max_item_total = find_small_max_item(source_items, sack.max_total_weight)) {
            cout << "error: the knapsack is empty but there is a small enough item with a positive value ("
                            << max_item_total.to_string() << ")" << endl;
            ok = false;
        }
    }

    // not a single valuable item left if there was enough space for it
    if (min_item.value > 0 && weight_left >= min_item.weight) {
        cout << "error: the knapsack has enough weight available for one more item (" << min_item.to_string()
                        << "), weight left:" << weight_left << endl;
        ok = false;
    }

    return ok;
}


typedef vector<string> string_vec;

string_vec split(const string& s, char delim) {
    string_vec elems;
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }

    if (elems.empty())
        elems.push_back(s);

    return elems;
}


int test_and_print_knapsack(const knapsack& sack, const item_vec& source_items, const string& type) {
    cout << "filled knapsack (" << type << "):" << endl;
    print_knapsack(sack);
    cout << endl;

    cout << "test results:" << endl;
    if (test_knapsack(sack, source_items))
        cout << "tests OK" << endl;

    cout << endl;

    return calculate_total_value(sack.items);
}


void run_assignments(const item_vec& source_items, unsigned max_weight) {
    cout << "source items (" << source_items.size() << "):" << endl;
    print_knapsack(knapsack(max_weight, source_items));
    cout << endl;

    knapsack top_down(max_weight);
    fill_knapsack_top_down(top_down, source_items);

    int top_down_value = test_and_print_knapsack(top_down, source_items, "top-down");

    knapsack bottom_up(max_weight);
    fill_knapsack_bottom_up(bottom_up, source_items);

    int bottom_up_value = test_and_print_knapsack(bottom_up, source_items, "bottom-up");

    if (top_down_value != bottom_up_value) {
        cout << "error: top-down and bottom-up results differ (" << top_down_value << " != " << bottom_up_value << ")" << endl;
    }
}


void print_help_and_exit(const char* me, const parameters& params) {
    cout << "Usage:" << endl;
    cout << "1. Specifying the items externally: "
                    << me << " -S W:w,v/w,v/..." << endl;
    cout << "   where w,v/w,v,... mean {weight,value} items separated by /" << endl;
    cout << "   For example, this will specify 2 sets of items and their knapsacks to be filled:" << endl;
    cout << "   " << me << " -S 10:1,5/2,3/4,12 -S 15:3,17/10,5" << endl;
    cout << "   Note: both the items and the knapsack should have positive weights." << endl;
    cout << endl;
    cout << "2. Generating random sets of items: "
                    << me << " -R max_knapsack_weight,number_of_items,[max_item_weight,max_item_value][:random_seed]" << endl;
    cout << "   defaults: max_knapsack_weight = " << params.knapsack_weight_max << endl;
    cout << "             number_of_items = " << params.num_items << endl;
    cout << "             max_item_weight = " << params.item_weight_max << endl;
    cout << "             max_item_value = " << params.item_value_max << endl;
    cout << "             random_seed = " << params.seed << endl;
    cout << endl;
    exit(0);
}


void print_arg_error_and_exit(const string& arg, const string& arg_val, const string& what) {
    cout << "error in arg " << arg << ": expected " << what << ", got '" << arg_val << "'" << endl;
    cout << "see --help for options" << endl;
    exit(1);
}


void print_arg_error_and_exit(const string& arg, const string& arg_val) {
    if ("-S" == arg) {
        print_arg_error_and_exit(arg, arg_val,
            "max_weight:item_weight,item_value/item_weight,item_value/... All weights are > 0");
    } else if ("-R" == arg) {
        print_arg_error_and_exit( arg, arg_val,
            "max_knapsack_weight,number_of_items,[max_item_weight,max_item_value][:random_seed]... All numbers are > 0");
    }

    print_arg_error_and_exit("unknown error (bug?)", arg_val);
}


int parse_int_token(const string& arg, const string& arg_value,
                    const string_vec& tokens, size_t off, int deflt, bool required = false)
{
    if (off < tokens.size()) {
        int result = atoi(tokens[off].c_str());

        if (!result && "0" != tokens[off]) {
            print_arg_error_and_exit(arg, arg_value);
        }

        return result;
    } else if (required) {
        print_arg_error_and_exit(arg, arg_value);
    }

    return deflt;
}


unsigned parse_unsigned_token(const string& arg, const string& arg_value,
                              const string_vec& tokens, size_t off, unsigned deflt, bool required = false)
{
    int result = parse_int_token(arg, arg_value, tokens, off, deflt, required);

    if (result <= 0) {
        print_arg_error_and_exit(arg, arg_value);
    }

    return result;
}


int parse_required_int_token(const string& arg, const string& arg_value, const string_vec& tokens, size_t off) {
    return parse_int_token(arg, arg_value, tokens, off, 0, true);
}


unsigned parse_required_unsigned_token(const string& arg, const string& arg_value, const string_vec& tokens, size_t off) {
    return parse_unsigned_token(arg, arg_value, tokens, off, 0, true);
}


void process_manual_input(const string& arg, const string& arg_value) {
    string_vec weight_and_items = split(arg_value, ':');

    unsigned knapsack_weight_max = parse_required_unsigned_token(arg, arg_value, weight_and_items, 0);

    if (weight_and_items.size() < 2) {
        print_arg_error_and_exit(arg, arg_value);
    }

    string_vec items = split(weight_and_items[1], '/');
    item_vec source_items;

    for (size_t j = 0, items_sz = items.size(); j < items_sz; ++j) {
        string_vec params = split(items[j], ',');

        unsigned weight = parse_required_unsigned_token(arg, arg_value, params, 0);
        int value = parse_required_int_token(arg, arg_value, params, 1);

        source_items.push_back(item(j + 1, weight, value));
    }

    run_assignments(source_items, knapsack_weight_max);
}


void process_random_generated_input(parameters params, const string& arg, const string& arg_value) {
    // max_knapsack_weight,number_of_items,[max_item_weight,max_item_value][:random_seed]
    if (!arg_value.empty()) {
        string_vec params_and_weight = split(arg_value, ':');

        params.seed = parse_unsigned_token(arg, arg_value, params_and_weight, 1, params.seed);

        string_vec params_raw = split(params_and_weight[0], ',');

        params.knapsack_weight_max = parse_unsigned_token(arg, arg_value, params_raw, 0, params.knapsack_weight_max);
        params.num_items = parse_unsigned_token(arg, arg_value, params_raw, 1, params.num_items);
        params.item_weight_max = parse_unsigned_token(arg, arg_value, params_raw, 2, params.item_weight_max);
        params.item_value_max = parse_unsigned_token(arg, arg_value, params_raw, 3, params.item_value_max);
    }

    for (unsigned i = 1; i <= params.num_items; ++i) {
        parameters cur_params = params;
        cur_params.num_items = i;
        run_assignments(generate_random_items(params, i == 1), cur_params.knapsack_weight_max);
    }
}


int main(int argc, const char** argv) {
    parameters params;

    if (argc < 2) {
        print_help_and_exit(argv[0], params);
    }

    for (int i = 1; i < argc; ++i) {
        string arg(argv[i]);

        if ("--help" == arg || "-h" == arg || "-?" == arg) {
            print_help_and_exit(argv[0], params);
        }

        if ("-S" == arg) {
            i += 1;
            string arg_value(i < argc ? argv[i] : "");
            process_manual_input(arg, arg_value);
        } else if ("-R" == arg) {
            string arg_value(i + 1 < argc ? argv[i + 1] : "");

            if (!arg_value.empty() && arg_value[0] != '-') {
                i += 1;
            }

            process_random_generated_input(params, arg, arg_value);
        }
    }
}
