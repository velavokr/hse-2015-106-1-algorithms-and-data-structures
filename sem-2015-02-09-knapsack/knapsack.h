#pragma once

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>
#include <vector>

namespace knapsack_task {

struct item {
    unsigned id;     // идентификатор
    unsigned weight; // вес
    int value;       // полезность

    item()
        : id()
        , weight()
        , value()
    {}

    item(unsigned item_id, unsigned item_weight, int item_value)
        : id(item_id)
        , weight(item_weight)
        , value(item_value)
    {}

    std::string to_string() const {
        std::stringstream ss;
        ss << "id:" << id << ",w:" << weight << ",v:" << value;
        return ss.str();
    }

    bool operator<(const item& other) const {
        return id < other.id;
    }

    bool operator==(const item& other) const {
        return id == other.id;
    }

    operator bool() const {
        return id;
    }
};

typedef std::vector<item> item_vec;


struct knapsack {
    unsigned max_total_weight;
    item_vec items;

    knapsack(unsigned weight, const item_vec& sack_items = item_vec())
        : max_total_weight(weight)
        , items(sack_items)
    {}
};


void fill_knapsack_top_down(knapsack& sack, item_vec items) {
    // Задание 1:
    // наполнить рюкзак методом top-down (через рекурсии и мемоизацию)
}


void fill_knapsack_bottom_up(knapsack& sack, item_vec items) {
    // Задание 2:
    // наполнить рюкзак методом bottom-up (через построение промежуточных решений в цикле)
}

}
