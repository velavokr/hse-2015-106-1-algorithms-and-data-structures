#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>

using namespace std;

// [start, finish]
struct task {
    unsigned start;  // отрезок времени включает момент start
    unsigned finish; // отрезок времени включает момент finish
    int value;       // полезность

    task(unsigned s, unsigned f, unsigned p)
        : start(min(s, f))
        , finish(max(s, f))
        , value(p)
    {}

    task()
        : start()
        , finish()
        , value()
    {}

    bool operator<(const task& other) const {
        if (finish < other.finish)
            return true;

        // these are unnessessary for the solution
        // but it's convenient to have a total ordering over data
        if (finish == other.finish) {
            if (start < other.start)
                return true;

            if (start == other.start)
                return value < other.value;
        }

        return false;
    }

    bool operator==(const task& other) const {
        return start == other.start && finish == other.finish && value == other.value;
    }

    bool compatible(const task& other) const {
        return finish < other.start || other.finish < start;
    }

    string to_string() const {
        stringstream ss;
        ss << "[" << start << "," << finish << "]@" << value;
        return ss.str();
    }
};

typedef vector<task> task_vec;

const int empty_value = numeric_limits<int>::min();


task_vec optimize_schedule_top_down(task_vec source_schedule) {
    // Задание 1:
    // оптимизировать расписание методом top-down (через рекурсии и мемоизацию)
    return source_schedule;
}

task_vec optimize_schedule_bottom_up(task_vec source_schedule) {
    // Задание 2:
    // оптимизировать расписание методом bottom-up (через построение вектора промежуточных решений в цикле)
    return source_schedule;
}

// test stuff

struct parameters {
    unsigned num_intervals;
    unsigned time_span;
    unsigned profit_max;
    int seed;

    parameters()
        : num_intervals(10)
        , time_span(100)
        , profit_max(100)
        , seed(0)
    {}

    parameters(unsigned n, unsigned t, unsigned p, int s)
        : num_intervals(n)
        , time_span(t)
        , profit_max(p)
        , seed(s)
    {}
};

typedef vector<unsigned> conflict;
typedef vector<conflict> conflict_vec;


task_vec generate_random_schedule(const parameters& params, bool reset_seed) {
    if (reset_seed)
        srand(params.seed);

    task_vec schedule;

    for (unsigned i = 0; i < params.num_intervals; ++i) {
        unsigned a = unsigned(rand()) % params.time_span;
        unsigned b = unsigned(rand()) % params.time_span;
        int v = rand() % (2 * params.profit_max) - params.profit_max;
        schedule.push_back(task(a, b, v));
    }

    return schedule;
}


void print_schedule(const task_vec& schedule) {
    streamsize width = log10(max<double>(schedule.size(), 1)) + 1;
    streamsize oldwidth = cout.width();

    for (size_t i = 0, sz = schedule.size(); i < sz; ++i) {
        task t = schedule[i];

        cout.width(width);
        cout << i;
        cout.width(oldwidth);

        cout << string(t.start, ' ') << ">" << string((t.finish - t.start), '>') << t.to_string() << endl;
    }
}


conflict_vec find_conflicts(const task_vec& schedule) {
    conflict_vec conflicts;
    conflicts.resize(schedule.size());
    for (size_t i = 0, sz = schedule.size(); i < sz; ++i) {
        task a = schedule[i];
        conflict& conflict = conflicts[i];

        for (size_t j = 0; j < sz; ++j) {
            if (i == j)
                continue;

            task b = schedule[j];

            if (!a.compatible(b)) {
                conflict.push_back(j);
            }
        }
    }

    return conflicts;
}


int calculate_total_value(const task_vec& final_schedule) {
    int result = 0;
    for (task_vec::const_iterator it = final_schedule.begin(); it != final_schedule.end(); ++it) {
        result += it->value;
    }

    return result;
}

int calculate_max_value(const task_vec& final_schedule) {
    int result = 0;
    for (task_vec::const_iterator it = final_schedule.begin(); it != final_schedule.end(); ++it) {
        result = max(result, it->value);
    }

    return result;
}


bool test_final_schedule(const task_vec& final_schedule, const task_vec& source_schedule) {
    bool ok = true;
    int source_max_value = calculate_max_value(source_schedule);
    int final_total_value = calculate_total_value(final_schedule);

    if (source_max_value > 0 && final_schedule.empty()) {
        cout << "error: the final schedule is empty but there are positive tasks in the source schedule" << endl;
        ok = false;
    }

    if (source_max_value > final_total_value) {
        cout << "error: the final schedule's value is less than the maximum task value ("
                        << final_total_value << " < " << source_max_value << ")" << endl;
        ok = false;
    }

    if (final_schedule.size() > source_schedule.size()) {
        cout << "error: the final schedule has more tasks than the source schedule ("
                    << final_schedule.size() << " > " << source_schedule.size() << ")" << endl;
        ok = false;
    }

    for (size_t i = 0, sz = final_schedule.size(); i < sz; ++i) {
        if (final_schedule[i].value <= 0) {
            cout << "error: the final schedule has a nonpositive value task at index "
                            << i << " (" << final_schedule[i].value << ") " << endl;
            ok = false;
        }
    }

    const conflict_vec& final_conflicts = find_conflicts(final_schedule);
    for (size_t i = 0, sz = final_conflicts.size(); i < sz; ++i) {
        const conflict& conf = final_conflicts[i];

        if (!conf.empty()) {
            cout << "error: the final schedule has a conflict between the task at index (in final) " << i << " and ";
            for (conflict::const_iterator it = conf.begin(); it != conf.end(); ++it) {
                if (it != conf.begin())
                    cout << ", ";
                cout << *it;
            }
            cout << endl;
            ok = false;
        }
    }

    const conflict_vec& source_conflicts = find_conflicts(source_schedule);
    for (size_t i = 0, sz = source_conflicts.size(); i < sz; ++i) {
        const conflict& conf = source_conflicts[i];

        if (conf.empty()) {
            task t = source_schedule[i];
            if (t.value > 0) {
                task_vec::const_iterator it = find(final_schedule.begin(), final_schedule.end(), t);
                if (it == final_schedule.end()) {
                    cout << "error: the final schedule doesn't contain the unconflicting"
                                    << " and positive source task at index (in source) " << i << endl;
                    ok = false;
                }
            }
        }
    }

    return ok;
}


typedef vector<string> string_vec;

string_vec split(const string &s, char delim) {
    string_vec elems;
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }

    return elems;
}


int test_and_output_final_schedule(const task_vec& final_schedule, const task_vec& source_schedule, const string& type) {
    cout << "optimized schedule (" << type << "):" << endl;
    print_schedule(final_schedule);
    cout << endl;

    cout << "test results:" << endl;
    if (test_final_schedule(final_schedule, source_schedule))
        cout << "tests OK" << endl;

    int result = calculate_total_value(final_schedule);
    cout << "schedule value: " << result << endl;

    cout << endl;

    return result;
}


void run_assignments(const task_vec& source_schedule) {
    cout << "source schedule:" << endl;
    print_schedule(source_schedule);
    cout << endl;

    int top_down = test_and_output_final_schedule(optimize_schedule_top_down(source_schedule), source_schedule, "top-down");
    int bottom_up = test_and_output_final_schedule(optimize_schedule_bottom_up(source_schedule), source_schedule, "bottom-up");

    if (top_down != bottom_up) {
        cout << "error: top-down and bottom-up results differ (" << top_down << " != " << bottom_up << ")" << endl;
    }
}

void print_help_and_exit(const char* me, unsigned num_intervals, int seed) {
    cout << "Usage:" << endl;
    cout << "1. Specifying the source schedule externally: "
                    << me << " -S s,f,v/s,f,v/..." << endl;
    cout << "   where s:f:v,s:f:v,... mean {[start,finish],value} items separated by /" << endl;
    cout << "   For example, this will specify 2 schedules to be solved:" << endl;
    cout << "   " << me << " -S 0,1,5/2,2,3/1,4,12 -S 3,3,17/1,10,5" << endl << endl;
    cout << "2. Generating random source schedules with sizes from 1 to max_sched_size: "
                    << me << " -R [[max_schedule_size[,random_seed]]" << endl;
    cout << "   defaults: max_schedule_size = " << num_intervals << ", random_seed = " << seed << endl << endl;
    exit(0);
}

int main(int argc, const char** argv) {
    unsigned num_intervals = 20;
    int seed = 1;

    if (argc < 2) {
        print_help_and_exit(argv[0], num_intervals, seed);
    }

    for (int i = 1; i < argc; ++i) {
        string arg(argv[i]);
        if ("--help" == arg || "-h" == arg || "-?" == arg) {
            print_help_and_exit(argv[0], num_intervals, seed);
        }

        if ("-S" == arg) {
            i += 1;
            string arg_value(i < argc ? argv[i] : "");
            string_vec tasks = split(arg_value, '/');
            task_vec source_schedule;

            for (string_vec::const_iterator it = tasks.begin(); it != tasks.end(); ++it) {
                string_vec params = split(*it, ',');

                if (params.size() != 3) {
                    cerr << "input error: an invalid schedule specification '" << arg_value << "'" << endl;
                    exit(1);
                }

                unsigned a = atoi(params[0].c_str());
                unsigned b = atoi(params[1].c_str());
                unsigned v = atoi(params[2].c_str());

                source_schedule.push_back(task(a, b, v));
            }

            run_assignments(source_schedule);
            continue;
        }

        if ("-R" == arg) {
            i += 1;
            string arg_value(i < argc ? argv[i] : "");

            if (!arg_value.empty() && arg_value[0] == '-')
                arg_value.clear();

            string_vec params_raw = split(arg_value, ',');

            if (params_raw.size() > 0) {
                num_intervals = atoi(params_raw[0].c_str());
            }

            if (params_raw.size() > 1) {
                seed = atoi(params_raw[1].c_str());
            }

            for (unsigned i = 1; i <= num_intervals; ++i) {
                parameters params;
                params.num_intervals = i;
                params.seed = seed;
                run_assignments(generate_random_schedule(params, i == 1));
            }
        }
    }
}
