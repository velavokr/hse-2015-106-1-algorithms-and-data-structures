#pragma once

#include <algorithm>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

namespace long_arithmetic {

    class long_uint {
        typedef unsigned chunk;
        typedef unsigned long long chunk2;

        static const chunk bits_low = 32;
        static const chunk2 mask_low = (chunk2(1) << bits_low) - 1;
        static const chunk2 mask_high = mask_low << bits_low;

        typedef std::vector<chunk> chunk_vec;
        typedef chunk_vec::const_iterator const_iter;
        typedef chunk_vec::iterator iter;

        chunk_vec chunks;

    public:
        static bool check_if_code_is_compatible() {
            return sizeof(chunk) * 2 <= sizeof(chunk2);
        }
        long_uint() {
            chunks.resize(1);
        }
        long_uint(const unsigned &number) {
            chunks.resize(1);
            chunks[0] = number;
        }
        long_uint(const unsigned long long &number) {
            chunks.resize(2);
            chunks[0] = number;
            chunks[1] = number >> bits_low;
        }

        void add(const long_uint& o) {
            add(0, o.chunks.begin(), o.chunks.end());
        }

        void mult(const long_uint& o) {
            mult_slow(o.chunks.begin(), o.chunks.end());
        }

        void swap(long_uint& o) {
            std::swap(chunks, o.chunks);
        }

        void load16(const std::string& str) {
            size_t chunk_size = sizeof(chunk)* 2;
            std::string str_begin((chunk_size - str.size() % chunk_size) % chunk_size, '0'),
                good_str = str_begin + str;

            size_t result_size = good_str.size() / chunk_size;
            chunks = chunk_vec(result_size);

            for (size_t i = 0; i < result_size; ++i) {
                std::string tmp(good_str.begin() + (result_size - i - 1) * chunk_size,
                    good_str.begin() + (result_size - i) * chunk_size);
                std::istringstream s(tmp);
                s >> std::hex >> chunks[i];
            }
        }

        std::string save16() const {
            std::ostringstream s;
            s << std::hex;

            for (const_iter it = chunks.end(); it != chunks.begin(); --it) {
                if (it == chunks.end()) {
                    s << std::setw(1);
                } else {
                    s << std::setw(sizeof(chunk)* 2) << std::setfill('0');
                }

                s << *(it - 1);
            }

            return s.str();
        }

        std::string debug_string() const {
            std::ostringstream s;
            s << std::hex << std::setfill('0');

            for (const_iter it = chunks.end(); it != chunks.begin(); --it) {
                if (it != chunks.end())
                    s << ":";
                s << std::setw(sizeof(chunk)* 2) << *(it - 1);
            }

            return s.str();
        }

    private:
        void add(size_t pos, long_uint::const_iter begin, long_uint::const_iter end) {
            for (const_iter it = begin; it != end; ++it) {
                add_one(pos + (it - begin), *it);
            }
        }

        void add_one(size_t pos, long_uint::chunk c) {
            size_t sz = chunks.size();

            for (; pos < sz; ++pos) {
                chunk2 d = static_cast<chunk2>(chunks[pos]) + static_cast<chunk2>(c);
                chunks[pos] = d;
                c = d >> bits_low;
            }

            if (c) {
                chunks.push_back(c);
            }
        }

        void mult_slow(long_uint::const_iter begin, long_uint::const_iter end) {
            long_uint result;
            result.chunks.reserve(end - begin + chunks.size());

            for (const_iter it = begin; it != end; ++it) {
                long_uint round = *this;

                round.mult_one(*it);
                result.add(it - begin, round.chunks.begin(), round.chunks.end());
            }

            swap(result);
        }

        void mult_one(long_uint::chunk c) {
            long_uint result;
            for (size_t i = 0, sz = chunks.size(); i < sz; ++i) {
                chunk2 d = static_cast<chunk2>(chunks[i]) * static_cast<chunk2>(c);
                result.add_one(i, d & mask_low);
                result.add_one(i + 1, d >> bits_low);
            }
            swap(result);
        }
    };

}
