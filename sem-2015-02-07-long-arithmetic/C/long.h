#ifndef _LONG_H_
#define _LONG_H_

#include <stdint.h>


// Числовые типы, в виде которых надо хранить инфу.
// LONG_DIGIT должен вмещать (2 * LONG_SHIFT + 1) бит, плюс знак.
#define LONG_SIZE  size_t
#define LONG_DIGIT int64_t

// LONG_SHIFT: сколько бит содержится в одной цифре.
// LONG_BASE:  2 ** LONG_SHIFT, т.е. основание системы счисления.
//             Тут степень двойки для ускорения; если взять побитовое И
//             с (LONG_BASE - 1), это будет как взятие по модулю, а смещение
//             вправо на LONG_SHIFT это деление.
#define LONG_SHIFT (sizeof(LONG_DIGIT) * 4 - 1)
#define LONG_BASE  ((LONG_DIGIT) (1LL << LONG_SHIFT))
#define LONG_MASK  (LONG_BASE - 1)

// Значение 1/log2(10), нужно чтобы прикинуть количество цифр при выводе. Оно равно
// log10(x) = log2(x) / log2(10) <= x->size * LONG_SHIFT * LONG_LOG2_10. Тут написано
// приближение сверху, так что символов хватит точно. Разве что пара лишних выделится.
#define LONG_LOG2_10 0.30103


typedef struct _struct_long_t * long_t;

long_t long_from_int(int);
long_t long_from_string(const char *);
long_t long_copy(long_t);
char * long_to_string(long_t);
void long_dealloc(long_t);

int long_cmp(long_t, long_t);
void long_add_inplace(long_t, long_t);
void long_sub_inplace(long_t, long_t);
void long_mul_inplace(long_t, long_t);

// Эти функции настолько простые, что выносить их в long.c не нужно.
static inline int long_lt(long_t x, long_t y) { return long_cmp(x, y) <  0; }
static inline int long_le(long_t x, long_t y) { return long_cmp(x, y) <= 0; }
static inline int long_eq(long_t x, long_t y) { return long_cmp(x, y) == 0; }
static inline int long_ne(long_t x, long_t y) { return long_cmp(x, y) != 0; }
static inline int long_ge(long_t x, long_t y) { return long_cmp(x, y) >= 0; }
static inline int long_gt(long_t x, long_t y) { return long_cmp(x, y) >  0; }
static inline long_t long_add(long_t x, long_t y) { if ((x = long_copy(x))) long_add_inplace(x, y); return x; }
static inline long_t long_sub(long_t x, long_t y) { if ((x = long_copy(x))) long_sub_inplace(x, y); return x; }
static inline long_t long_mul(long_t x, long_t y) { if ((x = long_copy(x))) long_mul_inplace(x, y); return x; }

#endif // _LONG_H_
