#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

#include "long.h"


struct _struct_long_t {
    short       sign;
    LONG_SIZE   size;
    LONG_DIGIT *digits;
};


long_t long_from_int(int value) {
    long_t x = (long_t) malloc(sizeof(struct _struct_long_t));

    if (x) {
        x->sign   = value < 0 ? -1 : 1;
        x->size   = 1;
        x->digits = (LONG_DIGIT *) malloc(sizeof(LONG_DIGIT));

        if (x->digits == NULL) {
            long_dealloc(x);  // Не хватило памяти на цифру,
            return NULL;      // ужас какой-то.
        }

        x->digits[0] = value < 0 ? -value : value;
    }

    return x;
}


long_t long_from_string(const char *data) {
    long_t x = long_from_int(0);
    long_t y = long_from_int(10);

    if (x) {
        while (*data) {
            if (*data < '0' || '9' < *data) {
                long_dealloc(x);
                long_dealloc(y);
                return NULL;
            }

            // XXX очень неоптимально. Можно парсить несколько цифр сразу.
            long_t z = long_from_int(*data++ - '0');
            long_mul_inplace(x, y);
            long_add_inplace(x, z);
            long_dealloc(z);
        }
    }

    long_dealloc(y);
    return x;
}


long_t long_copy(long_t x) {
    long_t y = (long_t) malloc(sizeof(struct _struct_long_t));

    if (y) {
        y->size = x->size;
        y->sign = x->sign;
        y->digits = (LONG_DIGIT *) malloc(sizeof(LONG_DIGIT) * y->size);

        if (y->digits == NULL) {
            long_dealloc(y);
            return NULL;
        }

        memcpy(y->digits, x->digits, sizeof(LONG_DIGIT) * y->size);
    }

    return y;
}


char * long_to_string(long_t x) {
    // По-хорошему тут нужно десятичное представление:
    // size_t length = floor(x->size * LONG_SHIFT * LONG_LOG2_10) + 3;  // +1 под знак, +1 под NUL
    // char * target = (char *) malloc(sizeof(char) * length);
    // А дальше что? Кажется, у Кнута был какой-то алгоритм...
    // Ладно, будем делать так. Будет просто список цифр, разделенных запятыми.
    size_t length = floor(x->size * LONG_SHIFT * LONG_LOG2_10) + x->size + 3;
    char * target = (char *) malloc(sizeof(char) * length);
    char * ptr = target;
    if (x->sign < 0) *ptr++ = '-';

    for (size_t i = 0; i < x->size; ++i) {
        ptr += snprintf(ptr, length - (ptr - target), "%" PRId64 ",", x->digits[i]);
    }

    *ptr = 0;
    return target;
}


void long_dealloc(long_t x) {
    if (x) {
        free(x->digits);
        free(x);
    }
}


int long_cmp(long_t x, long_t y) {
    if (x->size > y->size || (x->sign > 0 && y->sign < 0)) {
        return +1;
    }

    if (x->size < y->size || (x->sign < 0 && y->sign > 0)) {
        return -1;
    }

    for (size_t i = x->size; i; --i) {
        LONG_DIGIT cmp = (x->digits[i - 1] - y->digits[i - 1]) * x->sign;
        if (cmp > 0) return +1;
        if (cmp < 0) return -1;
    }

    return 0;
}


void long_resize_shift_inplace(long_t x, LONG_SIZE size, LONG_SIZE shift) {
    // Изменение размера числа без потери цифр.
    // При добавлении новых они выставляются в 0.
    // При этом перед началом числа добавляются еще `shift` нулей.
    LONG_DIGIT *old_digits = x->digits;
    LONG_SIZE   old_size   = x->size < size ? x->size : size;

    size += shift;

    if (size) {
        x->size   = size;
        x->digits = (LONG_DIGIT *) malloc(sizeof(LONG_DIGIT) * size);
        memset(x->digits, 0, sizeof(LONG_DIGIT) * size);
    } else {
        x->size   = size;
        x->digits = NULL;
    }

    if (old_digits) {
        memcpy(x->digits + shift, old_digits, old_size * sizeof(LONG_DIGIT));
        free(old_digits);
    }
}


void long_trim_inplace(long_t x) {
    size_t i;
    for (i = x->size; i && !x->digits[i - 1]; --i);
    long_resize_shift_inplace(x, i, 0);
}


void long_add_inplace(long_t x, long_t y) {
    if (x->sign < 0 && y->sign > 0) {
        // (-a) + b = -(a - b)
        x->sign *= -1;
        long_sub_inplace(x, y);
        x->sign *= -1;
        return;
    }

    if (x->sign > 0 && y->sign < 0) {
        // a + (-b) = a - b
        y->sign *= -1;
        long_sub_inplace(x, y);
        y->sign *= -1;  // `y` должен остаться прежним.
        return;
    }

    long_resize_shift_inplace(x, x->size > y->size ? x->size + 1 : y->size + 1, 0);
    LONG_DIGIT carry = 0;

    for (size_t i = 0; i < x->size; i++) {
        // assert(carry < LONG_BASE);
        x->digits[i] += i < y->size ? y->digits[i] + carry : carry;
        carry = x->digits[i] >> LONG_SHIFT;
        x->digits[i] &= LONG_MASK;
        // assert(x->digits[i] < LONG_BASE);
    }

    long_trim_inplace(x);
}


void long_sub_inplace(long_t x, long_t y) {
    LONG_DIGIT carry = 0;

    if (x->sign < 0 && y->sign > 0) {
        // (-a) - b = -(a + b)
        x->sign *= -1;
        long_add_inplace(x, y);
        x->sign *= -1;
        return;
    }

    if (x->sign > 0 && y->sign < 0) {
        // a - (-b) = a + b
        y->sign *= -1;
        long_add_inplace(x, y);
        y->sign *= -1;
        return;
    }

    if (long_gt(y, x)) {
        // a - b = -(b - a)
        // Немного неэффективно, но проще, чем разбираться с таким случаем внизу.
        long_t z = long_copy(y);
        long_sub_inplace(z, x);
        x->sign   = z->sign * -1;
        x->size   = z->size;
        x->digits = z->digits;
        z->digits = NULL;
        z->size   = 0;
        long_dealloc(z);
        return;
    }

    long_resize_shift_inplace(x, x->size + 1, 0);

    for (size_t i = 0; i < x->size; i++) {
        x->digits[i] -= i < y->size ? y->digits[i] + carry : carry;

        if (x->digits[i] < 0) {
            x->digits[i] += LONG_BASE;
            // Случай x < y уже обработали выше.
            carry = 1;
        } else {
            carry = 0;
        }
    }

    long_trim_inplace(x);
}


void long_mul_digit_inplace(long_t x, LONG_DIGIT p) {
    long_resize_shift_inplace(x, x->size + 1, 0);
    LONG_DIGIT carry = 0;

    for (size_t i = 0; i < x->size; i++) {
        x->digits[i] *= p;
        x->digits[i] += carry;
        carry = x->digits[i] >> LONG_SHIFT;
        x->digits[i] &= LONG_MASK;
    }

    long_trim_inplace(x);
}


void long_mul_inplace(long_t x, long_t y) {
    x->sign *= y->sign;

    long_t cpy = long_copy(x);
    long_resize_shift_inplace(x, 0, 0);

    for (size_t i = 0; i < y->size; i++) {
        long_t z = long_copy(cpy);
        long_mul_digit_inplace(z, y->digits[i]);
        long_resize_shift_inplace(z, z->size, i);
        long_add_inplace(x, z);
    }
}
