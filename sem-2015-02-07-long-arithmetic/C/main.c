#include <stdlib.h>
#include <stdio.h>

#include "long.h"


void long_printf(const char * fmt, long_t x) {
    char * buf = long_to_string(x);
    printf(fmt, buf);
    free(buf);
}


int main(int argc, char ** argv) {
    if (argc != 3) {
        fprintf(stderr, "usage: %s X Y\n", argv[0]);
        return 1;
    }

    long_t x = long_from_string(argv[1]); if (x == NULL) return 2;
    long_t y = long_from_string(argv[2]); if (x == NULL) return 2;
    long_t sum = long_add(x, y);
    long_t dif = long_sub(x, y);
    long_t mul = long_mul(x, y);

    printf("\n");
    long_printf("x = %s\n", x);
    long_printf("y = %s\n", y);
    printf("x >  y : %d\n", long_gt(x, y));
    printf("x >= y : %d\n", long_ge(x, y));
    printf("x == y : %d\n", long_eq(x, y));
    printf("x != y : %d\n", long_ne(x, y));
    printf("x <= y : %d\n", long_le(x, y));
    printf("x <  y : %d\n", long_lt(x, y));
    long_printf("x + y = %s\n", sum);
    long_printf("x - y = %s\n", dif);
    long_printf("x * y = %s\n", mul);

    long_dealloc(x);
    long_dealloc(y);
    long_dealloc(sum);
    long_dealloc(dif);
    long_dealloc(mul);
    return 0;
}
