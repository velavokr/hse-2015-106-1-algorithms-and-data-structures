from sys import argv
from random import randrange
from subprocess import check_call, check_output


def compile():
    check_call(['gcc', '-std=c99', '-Wall', '-Werror', '-lm', 'main.c', 'long.c', '-o', 'long'])


def parse(x):
    # Parse a single number in `a,b,c,...,` format,
    # where the number encoded is `a + (b << 31) + (c << 62) + ...`
    sign = -1 if x.startswith('-') else +1
    return sign * sum((int(s.lstrip('-')) << (31 * i)) for i, s in enumerate(x.split(',')) if s)


def parse_equality(x):
    # Parse a `... = number` line, return the number.
    return parse(x.split('=', 1)[1].strip())


def parse_comparison(x):
    # Parse a `... : [10]` line, return a boolean value.
    return x.split(':', 1)[1].strip() == '1'


def test(x, y):
    out = check_output(['./long', str(x), str(y)]).decode('ascii').split('\n')
    # Line 0 is empty.
    assert parse_equality(out[1]) == x, 'x invalid [{}]'.format(out[1])
    assert parse_equality(out[2]) == y, 'y invalid [{}]'.format(out[2])
    assert parse_comparison(out[3]) is (x >  y), 'x > y invalid'
    assert parse_comparison(out[4]) is (x >= y), 'x >= y invalid'
    assert parse_comparison(out[5]) is (x == y), 'x == y invalid'
    assert parse_comparison(out[6]) is (x != y), 'x != y invalid'
    assert parse_comparison(out[7]) is (x <= y), 'x <= y invalid'
    assert parse_comparison(out[8]) is (x <  y), 'x < y invalid'
    assert parse_equality(out[9])  == (x + y), 'x + y invalid [{}]'.format(out[9])
    assert parse_equality(out[10]) == (x - y), 'x - y invalid [{}]'.format(out[10])
    assert parse_equality(out[11]) == (x * y), 'x * y invalid [{}]'.format(out[11])


def random_test(limit=2 ** 100):
    x = randrange(0, limit)
    y = randrange(0, limit)
    print('x =', x)
    print('y =', y)
    test(x, y)
    print('ok')


if __name__ == '__main__':
    compile()
    n = 1 if len(argv) != 2 else int(argv[1])
    print('running', n, 'test(s)...')
    for _ in range(n):
        random_test()
