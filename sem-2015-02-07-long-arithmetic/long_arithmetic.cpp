#include "long_arithmetic.h"

#include <iostream>

using namespace long_arithmetic;

bool do_test_load_save_16(std::string s) {
    long_uint u;
    u.load16(s);
    std::string out = u.save16();

    if (out != s) {
        std::cout << "error: load16('" << s << "').save16() -> '"
                        << out << "' != '" << s << "' (expected) , debug: " << u.debug_string() << std::endl;
        return false;
    }

    std::cout << s << " -> " << u.debug_string() << std::endl;
    return true;
}

bool test_load_save_16() {
    std::string items[] = {
        "0", "1", "10", "ff", "1234567890abcdef", "1234567890abcdef1234567890abcdef", "123456789abcdef0123456789abcdef"
    };

    bool ok = true;

    for (size_t i = 0; i < sizeof(items) / sizeof(std::string); ++i) {
        ok &= do_test_load_save_16(items[i]);
    }

    return ok;
}

int main(int argc, const char** argv) {
    bool ok = true;

    ok &= test_load_save_16();

    if (ok) {
        std::cout << "tests OK" << std::endl;
    }
}
