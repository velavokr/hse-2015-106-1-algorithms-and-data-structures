#define JOIN(X, Y)     DO_JOIN(X, Y)
#define DO_JOIN(X, Y)  DO_JOIN2(X, Y)
#define DO_JOIN2(X, Y) X##Y

#define GENERATE_UNIQUE_ID(N) JOIN(N, __LINE__)

template <bool>
struct static_assertion_failed;

template <>
struct static_assertion_failed<true> {
};

template <int x>
struct static_assert_test {
};

#define STATIC_ASSERT(cond) \
    typedef static_assert_test<sizeof(static_assertion_failed<static_cast<bool>((cond))>)> GENERATE_UNIQUE_ID(TStaticAssertVar);

template <int>
struct compile_time_error;

template<>
struct compile_time_error<true> {
};

STATIC_ASSERT(2 * sizeof(unsigned) <= sizeof(unsigned long long))
