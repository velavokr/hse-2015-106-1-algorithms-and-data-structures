Код тестов находится в [sortings.cpp](sortings.cpp) , совместимый с 2003 стандартом находится в [sortings-old.cpp](sortings-old.cpp)

## Задание 1:
1) написать быструю сортировку

2) проверить, что быстрая сортировка проходит тест

3) проверить, что быстрая сортировка в среднем работает за O(n * log n)

4\*) сгенерировать тест, на котором быстрая сортировка будет работать за квадратичное время

## Задание 2:
1) написать сортировку вставками

2) проверить, что сортировка вставками проходит тест

3) проверить, что сортировка вставками в среднем работает за квадратичное время

4) проверить, что сортировка вставками на сортированной последовательности работает за линейное время

## Задание 3\*:
1) написать сортировку слиянием

2) проверить, что сортировка слиянием проходит тест

3) проверить, что сортировка слиянием работает за O(n * log n)