#include <algorithm>
#include <cassert>
#include <cmath>
#include <ctime>
#include <exception>
#include <functional>
#include <iomanip>
#include <iostream>
#include <utility>
#include <vector>

using namespace std;

void quick_sort(vector<int>::iterator /*begin*/, vector<int>::iterator /*end*/) {
    // Задание 1:
    // 1) написать быструю сортировку
    // 2) проверить, что быстрая сортировка проходит тесты
    // 3) проверить, что быстрая сортировка в среднем работает за O(n * log n)
    // 4*) сгенерировать тест, на котором быстрая сортировка будет работать за квадратичное время
}

void insertion_sort(vector<int>::iterator /*begin*/, vector<int>::iterator /*end*/) {
    // Задание 2:
    // 1) написать сортировку вставками
    // 2) проверить, что сортировка вставками проходит тесты
    // 3) проверить, что сортировка вставками в среднем работает за квадратичное время
    // 4) проверить, что сортировка вставками на сортированной последовательности работает за линейное время
}

void merge_sort(vector<int>::iterator /*begin*/, vector<int>::iterator /*end*/) {
    // Задание 3*:
    // 1) написать сортировку слиянием
    // 2) проверить, что сортировка слиянием проходит тесты
    // 3) проверить, что сортировка слиянием работает за O(n * log n)
}


vector<int> generate_sorted_sequence(size_t sz) {
    vector<int> result(sz);

    for (size_t i = 0; i < sz; ++i) {
        result[i] = i;
    }

    return result;
}

vector<int> generate_shuffled_sequence(size_t sz) {
    vector<int> vec = generate_sorted_sequence(sz);
    random_shuffle(vec.begin(), vec.end());
    return vec;
}

vector<int> generate_nasty_sequence_for_qsort(size_t sz) {
    // см. Задание 1.4*)
    return vector<int>();
}

double current_microseconds() {
    return clock() / double(CLOCKS_PER_SEC) * 1000000.;
};


void print_vector(const vector<int>& vec, ssize_t sorting_error = -1, ssize_t integrity_error = -1) {
    streamsize width = log10(max<double>(vec.size(), 1)) + 1;
    streamsize oldwidth = cout.width();

    cout << "[";
    for (ssize_t i = 0, sz = vec.size(); i < sz; ++i) {
        if (i) {
            cout << ", ";

            if (!(i % 20))
                cout << "\n ";
        }

        if (sorting_error == i || integrity_error == i) {
            if (sorting_error == i)
                cout << "UNSORTED ";

            if (integrity_error == i)
                cout << "CORRUPTED ";

            cout << "FROM HERE>";
        }

        cout.width(width);
        cout << vec[i];
        cout.width(oldwidth);
    }

    cout << "]" << endl;
}

void check_for_errors(const vector<int>& actual_output, const vector<int>& input) {
    vector<int> expected_output = input;
    sort(expected_output.begin(), expected_output.end());

    ssize_t sorting_error = -1;
    ssize_t integrity_error = -1;

    for (size_t i = 1, sz = actual_output.size(); i < sz; ++i) {
        if (actual_output[i] < actual_output[i - 1]) {
            sorting_error = i;
            break;
        }
    }

    if (sorting_error < 0 && actual_output != expected_output) {
        for (size_t i = 0, sz = min(actual_output.size(), expected_output.size()); i < sz; ++i) {
            if (actual_output[i] != expected_output[i]) {
                integrity_error = i;
                break;
            }
        }

        if (integrity_error < 0 && expected_output.size() != actual_output.size()) {
            integrity_error = min(actual_output.size(), expected_output.size());
        }
    }

    if (sorting_error >= 0 || integrity_error >= 0) {
        cout << "==TESTS FAILED==" << endl;

        cout << "INPUT (" << input.size() << " elements):" << endl;
        print_vector(input);

        cout << "EXPECTED OUTPUT (" << expected_output.size() << " elements):" << endl;
        print_vector(expected_output);

        cout << "ACTUAL OUTPUT (" << actual_output.size() << " elements):" << endl;
        print_vector(actual_output, sorting_error, integrity_error);

        cout << "ERRORS:" << endl;

        if (sorting_error >= 0)
            cout << " * invalid sorting starting from offset " << sorting_error << endl;

        if (integrity_error >= 0)
            cout << " * corrupted result starting from offset " << integrity_error << endl;

        throw exception();
    }
}

template<typename sort_function>
double /*microseconds*/measure_sorting_time(sort_function sorter, vector<int>& seq) {
    vector<int> input = seq;

    double t = current_microseconds();
    sorter(seq.begin(), seq.end());
    t = current_microseconds() - t;

    check_for_errors(seq, input);
    return t;
}


template <typename sort_function, typename generator_function>
double measure_average_sorting_time(sort_function sorter, generator_function generator, size_t input_size, unsigned n_folds = 16) {
    double result = 0;

    for (unsigned f = 0; f < n_folds; ++f) {
        vector<int> vec = generator(input_size);
        result += measure_sorting_time(sorter, vec);
    }

    return result / n_folds;
}


void print_times(size_t sz, double microseconds) {
    size_t sz1 = max<size_t>(sz, 1);
    cout  << setw(7) << sz << scientific << setprecision(3) << "\t" << microseconds
               << "\t" << microseconds / sz1
               << "\t" << microseconds / sz1 / max(log((double)sz), 1.)
               << "\t" << microseconds / sz1 / sz1 << endl;
}

int main() {
    cout << "input\ttime\ttime / n\ttime / n / log(n)\ttime / n / n" << endl;

    cout << endl << "std::sort" << endl;

    for (size_t i = 0; i < 16u; ++i) {
        size_t sz = 1 << i;
        double microseconds = measure_average_sorting_time(sort<vector<int>::iterator>, generate_shuffled_sequence, sz);
        print_times(sz, microseconds);
    }

    cout << endl << "std::stable_sort" << endl;

    for (size_t i = 0; i < 10u; ++i) {
        size_t sz = 1 << i;
        double microseconds = measure_average_sorting_time(stable_sort<vector<int>::iterator>,
                                                           generate_shuffled_sequence, sz);
        print_times(sz, microseconds);
    }

    try {
        cout << endl << "quick_sort (average)" << endl;

        for (size_t i = 0; i < 10u; ++i) {
//            size_t sz = 1 << i;
//            double microseconds = measure_average_sorting_time(quick_sort, generate_shuffled_sequence, sz);
//            print_times(sz, microseconds);
        }
    } catch (const exception&) {
    }

    try {
        cout << endl << "quick_sort (worst)" << endl;

        for (size_t i = 0; i < 10u; ++i) {
//            size_t sz = 1 << i;
//            double microseconds = measure_average_sorting_time(quick_sort, generate_nasty_sequence_for_qsort, sz);
//            print_times(sz, microseconds);
        }
    } catch (const exception&) {
    }

    try {
        cout << endl << "insertion_sort (average)" << endl;

        for (size_t i = 0; i < 10u; ++i) {
//            size_t sz = 1 << i;
//            double microseconds = measure_average_sorting_time(insertion_sort, generate_shuffled_sequence, sz);
//            print_times(sz, microseconds);
        }
    } catch (const exception&) {
    }

    try {
        cout << endl << "insertion_sort (best)" << endl;

        for (size_t i = 0; i < 10u; ++i) {
//            size_t sz = 1 << i;
//            double microseconds = measure_average_sorting_time(insertion_sort, generate_sorted_sequence, sz);
//            print_times(sz, microseconds);
        }
    } catch (const exception&) {
    }

    try {
        cout << endl << "merge_sort" << endl;

        for (size_t i = 0; i < 10u; ++i) {
//            size_t sz = 1 << i;
//            double microseconds = measure_average_sorting_time(merge_sort, generate_shuffled_sequence, sz);
//            print_times(sz, microseconds);
        }
    } catch (const exception&) {
    }
}
