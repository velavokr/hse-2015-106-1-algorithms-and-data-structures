#include <cassert>
#include <cstdint>

#include <iostream>
#include <vector>

using namespace std;

class State {
    uint64_t moves = 0;
    vector<uint32_t> towers[3];

public:
    explicit State(size_t size) {
        while (size) {
            towers[0].push_back(size--);
        }
    }

    size_t size() const {
        return towers[0].size() + towers[1].size() + towers[2].size();
    }

    void move(uint32_t from, uint32_t to) {
        assert_move(from, to);
        towers[to].push_back(towers[from].back());
        towers[from].pop_back();
        ++moves;
    }

    void assert_move(uint32_t from, uint32_t to) const {
        assert(from != to);
        assert(from < 3);
        assert(to < 3);
        assert(!towers[from].empty());
        assert(towers[to].empty() || towers[to].back() > towers[from].back());
    }

    void print_state(ostream& out) const {
        out << moves << ":";
        for (const auto& tower : towers) {
            out << " [";
            for (auto disk : tower) {
                out << disk << ",";
            }
            out << "]";
        }
        out << endl;
    }
};

void do_solve(State& state, ostream& out, size_t size, uint32_t from, uint32_t tmp, uint32_t to) {
    if (size) {
        do_solve(state, out, size - 1, from, to, tmp);
        state.move(from, to);
        state.print_state(out);
        do_solve(state, out, size - 1, tmp, from, to);
    }
}

void solve(State& state, ostream& out) {
    state.print_state(out);
    do_solve(state, out, state.size(), 0, 1, 2);
}

int main() {
    uint32_t n = 0;
    cin >> n;
    State state(n);
    solve(state, cout);
}
