[Статья в en:wiki о ханойских башнях](http://en.wikipedia.org/wiki/Tower_of_Hanoi)

## Решение задачи о ханойских башнях
### Идея:

![Оптимальное решение для 4 дисков](http://mathforum.org/mathimages/imgUpload/ToH4.gif)

### Алгоритм:
```python
def solve_hanoi(from, tmp, to):
    def move_top(sz, from, tmp, to):
		move_top(sz - 1, from, to, tmp)    # переместили верх пирамидки из from в tmp, используя to вместо tmp, чтобы высвободить нижний диск
		to.append(from.pop())              # переместили нижний диск из from в to
		move_top(sz - 1, tmp, from, to)    # переместили верх пирамидки из tmp в to, используя from вместо tmp
	move_top(len(from), from, tmp, to)
```

### Реализация:

[Реализация на C++ (причёсанное решение с семинара)](hanoi.cpp)
