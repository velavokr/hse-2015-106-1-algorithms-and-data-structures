#pragma once

#include "task_6.h"

namespace task_6 {

int median3(iter begin, iter end);

iter_range range_partition_cormen_2pass(iter begin, iter end, int pivot);

void quick_sort_rand(iter begin, iter end);

}
