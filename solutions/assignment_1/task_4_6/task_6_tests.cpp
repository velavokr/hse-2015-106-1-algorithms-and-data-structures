#include "task_6_tests.h"
#include "quick_sort_bulletproof.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <iostream>

namespace task_6 {

test_variant generate_random_counts(size_t total_size) {
    test_variant result;

    int value = 0;

    while (total_size) {
        size_t count = 1 + (rand() % total_size);
        result.push_back(value_item(value, count));
        total_size -= count;
        ++value;
    }

    return result;
}


test_variant generate_random_values_exp(size_t step, size_t total_size0) {
    test_variant result;

    size_t total_size = total_size0;

    while (total_size) {
        size_t count = max<size_t>(total_size / step, 1);
        result.push_back(value_item(rand() % (total_size0 * total_size0), count));
        total_size -= count;
    }

    return result;
}


test_variant generate_random_values_lin(size_t total_size0) {
    test_variant result;

    size_t total_size = total_size0;

    while (total_size) {
        result.push_back(value_item(total_size, 1));
        total_size -= 1;
    }

    return result;
}


test_variant generate_random_alls(size_t total_size0) {
    test_variant result;

    size_t total_size = total_size0;

    while (total_size) {
        size_t count = 1 + (rand() % total_size);
        result.push_back(value_item(rand() % (total_size0 * total_size0), count));
        total_size -= count;
    }

    sort(result.begin(), result.end());

    return result;
}


test_variant_vec generate_test_variants(test_parameters params) {
    test_variant_vec result;

    for (size_t sz = 1; sz <= params.max_total_size; sz <<= 1) {
        for (int r = (int)(sz - params.neighbourhood_radius); r <= (int)(sz + params.neighbourhood_radius); ++r) {
            if (r < 0) {
                continue;
            }

            for (size_t ra = 0; ra < params.random_alls_count; ++ra) {
                result.push_back(generate_random_alls(r));
            }

            for (size_t rc = 0; rc < params.random_counts_count; ++rc) {
                result.push_back(generate_random_counts(r));
            }

            for (size_t rv = 0; rv < params.random_values_count; ++rv) {
                for (size_t step = 1; step < params.steps_count; ++step) {
                    result.push_back(generate_random_values_exp(step, r));
                }

                result.push_back(generate_random_values_lin(r));
            }
        }
    }

    sort(result.begin(), result.end());
    result.resize(unique(result.begin(), result.end()) - result.begin());

    return result;
}


int_vec generate_int_vec(const test_variant& var) {
    int_vec result;

    for (test_variant::const_iterator it = var.begin(); it != var.end(); ++it) {
        result.resize(result.size() + it->count, it->value);
    }

    return result;
}


bool test_case_less(const int_vec& a, const int_vec& b) {
    if (a.size() < b.size()) {
        return true;
    }

    if (a.size() > b.size()) {
        return false;
    }

    return a < b;
}


test_case_vec generate_test_cases(test_parameters params, const test_variant_vec& variants) {
    test_case_vec result;
    size_t step_size = 2 + params.shuffles_count;
    result.reserve(variants.size() * step_size);

    for (test_variant_vec::const_iterator it = variants.begin(); it != variants.end(); ++it) {
        size_t sz = result.size();

        result.resize(sz + step_size, generate_int_vec(*it));

        int_vec& sorted = result[sz];
        sort(sorted.begin(), sorted.end());

        int_vec& reversed = result[sz + 1];
        sort(reversed.begin(), reversed.end());
        reverse(reversed.begin(), reversed.end());

        for (size_t i = 0; i < params.shuffles_count; ++i) {
            int_vec& shuffled = result[sz + 2 + i];
            random_shuffle(shuffled.begin(), shuffled.end());
        }
    }

    sort(result.begin(), result.end(), test_case_less);
    result.resize(unique(result.begin(), result.end()) - result.begin());

    return result;
}


void print_vector(const int_vec& vec, size_t error = -1) {
    streamsize width = log10(max<double>(vec.size() * 2 + 1, 1)) + 1;
    streamsize oldwidth = cout.width();

    cout << "[";
    for (size_t i = 0, sz = vec.size(); i < sz; ++i) {
        if (i) {
            cout << ", ";

            if (!(i % 20))
                cout << endl << " ";
        }

        if (i == error) {
            cout << "@";
        }

        cout.width(width);
        cout << vec[i];
        cout.width(oldwidth);
    }

    cout << "]" << endl;
}


typedef pair<int_vec, int_vec> losses_and_garbage;

losses_and_garbage find_data_corruption(int_vec reference, int_vec result) {
    losses_and_garbage corruptions;

    sort(reference.begin(), reference.end());
    sort(result.begin(), result.end());

    if (reference == result) {
        return corruptions;
    }

    size_t i_ref = 0;
    size_t i_act = 0;
    size_t sz_ref = reference.size();
    size_t sz_act = result.size();
    size_t sz_min = min(sz_ref, sz_act);

    while (i_ref < sz_min && i_act < sz_min) {
        int ref = reference[i_ref];
        int act = result[i_act];

        if (ref == act) {
            ++i_ref;
            ++i_act;
        } else if (ref < act) {
            corruptions.first.push_back(ref);
            ++i_ref;
        } else if (ref > act) {
            corruptions.second.push_back(act);
            ++i_act;
        }
    }

    corruptions.first.insert(corruptions.first.end(), reference.begin() + i_ref, reference.begin() + sz_ref);
    corruptions.second.insert(corruptions.second.end(), result.begin() + i_act, result.begin() + sz_act);

    return corruptions;
}


size_t find_sorting_error(const int_vec& actual) {
    size_t i = 1;

    for (size_t sz = actual.size(); i < sz; ++i) {
        if (actual[i] < actual[i - 1]) {
            return i;
        }
    }

    return actual.size();
}


bool test_quick_sort(quick_sort impl, const int_vec& data) {
    int_vec actual = data;
    impl(actual.begin(), actual.end());

    losses_and_garbage corrupted = find_data_corruption(data, actual);

    bool ok = true;

    if (!corrupted.first.empty()) {
        cout << "error: some elements were lost" << endl;
        print_vector(corrupted.first);
        ok = false;
    }

    if (!corrupted.second.empty()) {
        cout << "error: some elements were unexpected" << endl;
        print_vector(corrupted.second);
        ok = false;
    }

    size_t sorting_error = find_sorting_error(actual);
    if (sorting_error != actual.size()) {
        cout << "error: bad sorting at index " << sorting_error << endl;
        print_vector(actual, sorting_error);
        ok = false;
    }

    cout << "input:" << endl;
    print_vector(data);

    if (!ok) {
        cout << "output:" << endl;
        print_vector(actual);
    }

    return ok;
}


bool test_quick_sort(bool fail_fast, quick_sort impl, const test_case_vec& tests) {
    size_t ok = 0;
    size_t failed = 0;
    for (test_case_vec::const_iterator it = tests.begin(); it != tests.end(); ++it) {
        if (test_quick_sort(impl, *it)) {
            cout << "test " << ok << " passed" << endl;
            ++ok;
        } else {
            ++failed;
            cout << "test " << failed << " failed" << endl;
            if (fail_fast) {
                break;
            }
        }
        cout << "----------------" << endl;
    }

    cout << ok << " tests passed" << endl;
    cout << failed << " tests failed" << endl;
    return !failed;
}


}
