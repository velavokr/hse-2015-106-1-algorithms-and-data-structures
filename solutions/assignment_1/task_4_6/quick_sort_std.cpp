#include "quick_sort_std.h"

#include <algorithm>

namespace task_6 {

void quick_sort_std(iter begin, iter end) {
    sort(begin, end);
}

}
