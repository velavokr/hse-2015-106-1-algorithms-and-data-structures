#pragma once

#include <vector>

namespace task_6 {

using namespace std;

typedef vector<int> int_vec;
typedef int_vec::iterator iter;
typedef pair<iter, iter> iter_range;

typedef void (*quick_sort)(iter, iter);

}
