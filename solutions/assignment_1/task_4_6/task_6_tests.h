#include "task_6.h"

namespace task_6 {

struct test_parameters {
    size_t max_total_size;
    size_t neighbourhood_radius;
    size_t steps_count;
    size_t shuffles_count;
    size_t random_values_count;
    size_t random_counts_count;
    size_t random_alls_count;

    test_parameters()
        : max_total_size(1u << 10)
        , neighbourhood_radius(5)
        , steps_count(5)
        , shuffles_count(5)
        , random_values_count(5)
        , random_counts_count(5)
        , random_alls_count(5)
    {}
};


struct value_item {
    int value;
    size_t count;

    value_item()
        : value()
        , count()
    {}

    value_item(int v, size_t c)
        : value(v)
        , count(c)
    {}

    pair<int, size_t> as_pair() const {
        return make_pair(value, count);
    }

    bool operator<(const value_item& other) const {
        return as_pair() < other.as_pair();
    }

    bool operator==(const value_item& other) const {
        return as_pair() == other.as_pair();
    }
};


typedef vector<value_item> test_variant;
typedef vector<test_variant> test_variant_vec;

typedef vector<int_vec> test_case_vec;

test_variant generate_random_counts(size_t total_size);
test_variant generate_random_values_exp(size_t step, size_t total_size);
test_variant generate_random_alls(size_t total_size);
test_variant generate_random_values_lin(size_t total_size);

int_vec generate_int_vec(const test_variant&);

test_variant_vec generate_test_variants(test_parameters);

test_case_vec generate_test_cases(test_parameters, const test_variant_vec&);

bool test_quick_sort(bool fail_fast, quick_sort, const test_case_vec&);

}
