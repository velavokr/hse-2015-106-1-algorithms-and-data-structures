#include "quick_sort_std.h"
#include "quick_sort_rand.h"
#include "merge_sort.h"
#include "task_6_tests.h"

#include <iostream>

int main() {
    using namespace task_6;
    test_parameters params;
    test_variant_vec variants = generate_test_variants(params);

    std::cout << variants.size() << " test variants to go" << std::endl;

    test_case_vec tests = generate_test_cases(params, variants);

    std::cout << tests.size() << " test cases to go" << std::endl << std::endl;

    std::cout << "quick_sort rand" << endl;

    if (!test_quick_sort(true, quick_sort_rand, tests)) {
        std::cout << "some tests failed" << std::endl;
        return 0;
    } else {
        std::cout << "all tests passed" << std::endl;
    }

    std::cout << "##############" << endl;

    std::cout << "merge_sort" << endl;

    if (!test_quick_sort(true, merge_sort, tests)) {
        std::cout << "some tests failed" << std::endl;
        return 0;
    } else {
        std::cout << "all tests passed" << std::endl;
    }

    return 0;
}
