#include "merge_sort.h"

namespace task_6 {

void do_copy(iter begin, iter end, iter out) {
    while (begin != end) {
        *out = *begin;
        ++begin;
        ++out;
    }
}


void do_merge(iter begin1, iter end1, iter begin2, iter end2, iter out) {
    while (begin1 != end1 && begin2 != end2) {
        if (*begin1 < *begin2) {
            *out = *begin1;
            ++begin1;
        } else {
            *out = *begin2;
            ++begin2;
        }

        ++out;
    }

    do_copy(begin1, end1, out);
    do_copy(begin2, end2, out);
}


void do_merge_sort(iter begin, iter end, iter buffer) {
    if (end - begin < 2) {
        return;
    }

    size_t len = end - begin;
    size_t len_2 = len / 2;

    do_merge_sort(begin, begin + len_2, buffer);
    do_merge_sort(begin + len_2, end, buffer);

    do_merge(begin, begin + len_2, begin + len_2, end, buffer);
    do_copy(buffer, buffer + len, begin);
}


void merge_sort(iter begin, iter end) {
    int_vec buffer(end - begin);
    do_merge_sort(begin, end, buffer.begin());
}

}
