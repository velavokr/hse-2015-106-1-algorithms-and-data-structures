#include "quick_sort_rand.h"

#include <cstdlib>
#include <functional>
#include <iostream>

namespace task_6 {

template <typename condition>
iter do_partition_cormen_condition(iter begin, iter end, condition cond) {
    iter small_end = begin;

    while (true) {
        if (begin == end) {
            return small_end;
        }

        if (cond(*begin)) {
            iter_swap(small_end, begin);
            ++small_end;
        }

        ++begin;
    }
}


iter partition_cormen(iter begin, iter end, int pivot) {
    return do_partition_cormen_condition(begin, end, bind2nd(less<int>(), pivot));
}


iter_range range_partition_cormen_2pass(iter begin, iter end, int pivot) {
    begin = partition_cormen(begin, end, pivot);
    return iter_range(begin, do_partition_cormen_condition(begin, end, bind2nd(equal_to<int>(), pivot)));
}


int median3(iter begin, iter end) {
    int first = *begin;
    int middle = *(begin + (end - begin) / 2);
    int last = *(end - 1);

    // first ? middle, first ? last, middle ? last
    if (first > middle) {
        swap(first, middle);
    }

    // first <= middle, first ? last, middle ? last
    if (first > last) {
        swap(first, last);
    }


    // first <= middle, first <= last, middle ? last
    if (middle > last) {
        swap(middle, last);
    }

    return middle;
}


void quick_sort_rand(iter begin, iter end) {
    if (end - begin < 2) {
        return;
    }

    if (end - begin == 2) {
        if (*begin >= *(begin + 1)) {
            iter_swap(begin, begin + 1);
        }

        return;
    }

    const size_t critical_len = max<size_t>(3 * (end - begin) / 4, 32);

    iter_range pivot_range = range_partition_cormen_2pass(begin, end, median3(begin, end));

    while (max<size_t>(pivot_range.first - begin, end - pivot_range.second) > critical_len) {
        pivot_range = range_partition_cormen_2pass(begin, end, *(begin + (rand() % (end - begin))));
    }

    quick_sort_rand(begin, pivot_range.first);
    quick_sort_rand(pivot_range.second, end);
}


}
