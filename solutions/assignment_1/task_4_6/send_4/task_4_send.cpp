#include <cstdlib>
#include <iostream>
#include <vector>

namespace task_4 {

using namespace std;

typedef vector<int> int_vec;
typedef int_vec::iterator iter;
typedef pair<iter, iter> iter_range;


void do_copy(iter begin, iter end, iter out) {
    while (begin != end) {
        *out = *begin;
        ++begin;
        ++out;
    }
}


void do_merge(iter begin1, iter end1, iter begin2, iter end2, iter out) {
    while (begin1 != end1 && begin2 != end2) {
        if (*begin1 < *begin2) {
            *out = *begin1;
            ++begin1;
        } else {
            *out = *begin2;
            ++begin2;
        }

        ++out;
    }

    do_copy(begin1, end1, out);
    do_copy(begin2, end2, out);
}


void do_merge_sort(iter begin, iter end, iter buffer) {
    if (end - begin < 2) {
        return;
    }

    size_t len = end - begin;
    size_t len_2 = len / 2;

    do_merge_sort(begin, begin + len_2, buffer);
    do_merge_sort(begin + len_2, end, buffer);

    do_merge(begin, begin + len_2, begin + len_2, end, buffer);
    do_copy(buffer, buffer + len, begin);
}


void merge_sort(iter begin, iter end) {
    int_vec buffer(end - begin);
    do_merge_sort(begin, end, buffer.begin());
}

}


int main() {
    using namespace std;
    using namespace task_4;

    size_t n = 0;
    cin >> n;

    int_vec data(n);
    for (size_t i = 0; i < n; ++i) {
        cin >> data[i];
    }

    merge_sort(data.begin(), data.end());

    for (iter it = data.begin(); it != data.end(); ++it) {
        if (it != data.begin())
            cout << " ";
        cout << *it;
    }
}
