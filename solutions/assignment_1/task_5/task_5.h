#pragma once

#include <vector>

namespace task_5 {

using namespace std;

typedef vector<int> int_vec;
typedef int_vec::iterator iter;
typedef pair<iter, iter> iter_range;

typedef iter (*partition)(iter, iter, int);
typedef iter_range (*range_partition)(iter, iter, int);

}
