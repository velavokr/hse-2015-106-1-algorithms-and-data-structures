#include "task_5_tests.h"

#include <algorithm>
#include <cmath>
#include <cstdlib>

namespace task_5 {

static void generate_pivot_sizes(test_variant_vec& result, test_parameters params, int n, int i) {
    for (int k = 0; k <= (int)params.boundary_check_size; ++k) {
        if (i + k <= n) {
            // i
            result.push_back(test_variant(n, i, k));
        }

        if (n - i - k >= 0) {
            result.push_back(test_variant(n, i, n - i - k));
        }
    }

    if (n - i > (int)params.boundary_check_size) {
        for (unsigned r = 0; r < params.random_checks_count; ++r) {
            int k = rand() % (n - i + 1);
            result.push_back(test_variant(n, i, k));
        }
    }
}


test_variant_vec generate_test_variants(test_parameters params) {
    test_variant_vec result;

    for (size_t sz = 1; sz <= params.max_size; sz <<= 1) {
        for (int n = (int)sz - (int)params.neighbourhood_radius; n <= (int)(sz + params.neighbourhood_radius); ++n) {
            if (n < 0) {
                continue;
            }

            // pivot_begin
            for (int i = 0; i <= (int)params.boundary_check_size; ++i) {
                if (i <= n) {
                    // near the left side of array
                    generate_pivot_sizes(result, params, n, i);
                    // near the right side of array
                    generate_pivot_sizes(result, params, n, n - i);
                }
            }

            if (n <= 2 * (int)params.boundary_check_size) {
                // no point in random generating
                continue;
            }

            for (unsigned r = 0; r < params.random_checks_count; ++r) {
                size_t i = params.boundary_check_size + 1 + (rand() % (n - 2 * params.boundary_check_size));
                generate_pivot_sizes(result, params, n, i);
                generate_pivot_sizes(result, params, n, n - i);
            }
        }
    }

    sort(result.begin(), result.end());
    result.resize(unique(result.begin(), result.end()) - result.begin());

    return result;
}


test_case generate_sorted_input(test_variant test) {
    test_case result(test, data_type_sorted);

    result.data.resize(test.size);

    for (size_t i = 0; i < test.size; ++i) {
        result.data[i] = 2 * i + 1;
    }

    if (test.pivot_begin < test.size) {
        if (test.pivot_size > 0) {
            result.pivot = result.data[test.pivot_begin];
        } else {
            result.pivot = result.data[test.pivot_begin] - 1;
        }
    } else {
        result.pivot = result.data.empty() ? 0 : result.data.back() + 1;
    }

    for (size_t i = 1; i < test.pivot_size; ++i) {
        result.data[test.pivot_begin + i] = result.pivot;
    }

    return result;
}


test_case reverse_input(test_case input) {
    input.type = data_type_reverse_sorted;
    reverse(input.data.begin(), input.data.end());
    return input;
}


test_case shuffle_input(test_case input) {
    input.type = data_type_shuffled;
    random_shuffle(input.data.begin(), input.data.end());
    return input;
}


test_case_vec generate_test_cases(test_parameters params, const test_variant_vec& variants) {
    test_case_vec result;
    result.reserve(variants.size() * (2 + params.shuffles_count));

    for (test_variant_vec::const_iterator it = variants.begin(); it != variants.end(); ++it) {
        const test_case& sorted = generate_sorted_input(*it);
        result.push_back(sorted);
        result.push_back(reverse_input(sorted));

        for (unsigned r = 0; r < params.shuffles_count; ++r) {
            result.push_back(shuffle_input(sorted));
        }
    }

    sort(result.begin(), result.end());
    result.resize(unique(result.begin(), result.end()) - result.begin());

    test_case c(test_variant(2, 0, 1), data_type_reverse_sorted);
    c.data.push_back(2);
    c.data.push_back(0);
    c.pivot = 0;
    result.insert(result.begin(), c);
    return result;
}


size_t find_partition_offset(const test_case& test) {
    for (size_t i = 0, sz = test.data.size(); i < sz; ++i) {
        if (test.data[i] >= test.pivot) {
            return i;
        }
    }

    return test.data.size();
}


size_t find_partition_size(size_t off, const test_case& test) {
    for (size_t i = off, sz = test.data.size(); i < sz; ++i) {
        if (test.data[i] != test.pivot) {
            return i - off;
        }
    }

    return test.data.size() - off;
}


typedef pair<int_vec, int_vec> losses_and_garbage;


losses_and_garbage find_data_corruption(test_case reference, test_case result) {
    losses_and_garbage corruptions;

    sort(reference.data.begin(), reference.data.end());
    sort(result.data.begin(), result.data.end());

    if (reference.data == result.data) {
        return corruptions;
    }

    size_t i_ref = 0;
    size_t i_act = 0;
    size_t sz_ref = reference.data.size();
    size_t sz_act = result.data.size();
    size_t sz_min = min(sz_ref, sz_act);

    while (i_ref < sz_min && i_act < sz_min) {
        int ref = reference.data[i_ref];
        int act = result.data[i_act];

        if (ref == act) {
            ++i_ref;
            ++i_act;
        } else if (ref < act) {
            corruptions.first.push_back(ref);
            ++i_ref;
        } else if (ref > act) {
            corruptions.second.push_back(act);
            ++i_act;
        }
    }

    corruptions.first.insert(corruptions.first.end(), reference.data.begin() + i_ref, reference.data.begin() + sz_ref);
    corruptions.second.insert(corruptions.second.end(), result.data.begin() + i_act, result.data.begin() + sz_act);

    return corruptions;
}


size_t find_pivot_error(const test_case& result, bool range) {
    for (size_t i = 0, sz = result.data.size(); i < sz; ++i) {
        if (range) {
            if ((i < result.pivot_begin && result.data[i] >= result.pivot)
                            || (i >= result.pivot_begin
                                            && i < (result.pivot_begin + result.pivot_size)
                                            && result.data[i] != result.pivot)
                            || (i >= (result.pivot_begin + result.pivot_size)
                                            && result.data[i] <= result.pivot))
            {
                return i;
            }
        } else {
            if (i < result.pivot_begin && result.data[i] >= result.pivot) {
                return i;
            }

            if (i >= result.pivot_begin && result.data[i] < result.pivot) {
                return i;
            }
        }
    }

    return -1;
}


void print_vector(const int_vec& vec, size_t partition_offset, size_t partition_size, bool print_partition, size_t error = -1) {
    streamsize width = log10(max<double>(vec.size() * 2 + 1, 1)) + 1;
    streamsize oldwidth = cout.width();

    cout << "[";
    for (size_t i = 0, sz = vec.size(); i < sz; ++i) {
        if (print_partition && partition_size && i == partition_offset + partition_size) {
            cout << ")";
        }

        if (i) {
            cout << ", ";

            if (!(i % 20))
                cout << endl << " ";
        }

        if (print_partition) {
            if (i == partition_offset) {
                cout << "|";

                if (!partition_size) {
                    cout << ")";
                }
            }

            if (i == error) {
                cout << "@";
            }
        }

        cout.width(width);
        cout << vec[i];
        cout.width(oldwidth);
    }

    if (print_partition) {
        if (partition_offset == vec.size()) {
            cout << "|";
        }

        if (partition_offset + partition_size == vec.size()) {
            cout << ")";
        }
    }

    cout << "]" << endl;
}


bool assert_partition(bool range, const test_case& reference, const test_case& result) {
    losses_and_garbage corrupted = find_data_corruption(reference, result);
    bool ok = true;

    if (!corrupted.first.empty()) {
        cout << "error: some elements were lost" << endl;
        print_vector(corrupted.first, 0, 0, false);
        ok = false;
    }

    if (!corrupted.second.empty()) {
        cout << "error: some elements were unexpected" << endl;
        print_vector(corrupted.second, 0, 0, false);
        ok = false;
    }

    size_t pivot_error = find_pivot_error(result, range);
    if (pivot_error != size_t(-1)) {
        cout << "error: bad partitioning at index " << pivot_error << endl;
        print_vector(result.data, result.pivot_begin, result.pivot_size, true, pivot_error);
        ok = false;
    }

    size_t pivot_begin = find_partition_offset(result);
    if (pivot_begin != result.pivot_begin) {
        cout << "error: actual partition offset differs from the function return (actual "
                        << pivot_begin << " != returned " << result.pivot_begin << ")" << endl;
        ok = false;
    }

    if (pivot_begin != reference.pivot_begin) {
        cout << "error: actual partition offset differs from expected (actual "
                        << pivot_begin << " != expected " << reference.pivot_begin << ")" << endl;
        ok = false;
    }

    if (range) {
        size_t pivot_size = find_partition_size(pivot_begin, result);

        if (pivot_size != result.pivot_size) {
            cout << "error: actual partition size differs from the function return (actual "
                            << pivot_size << " != returned " << result.pivot_size << ")" << endl;
            ok = false;
        }

        if (pivot_size != reference.pivot_size) {
            cout << "error: actual partition size differs from expected (actual "
                            << pivot_size << " != expected " << reference.pivot_size << ")" << endl;
            ok = false;
        }
    }

    return ok;
}


void print_test(const test_case& reference, const test_case& result) {
    cout << "pivot: " << reference.pivot << endl;
    cout << "input data (size " << reference.data.size() << "):" << endl;
    print_vector(reference.data, reference.pivot_begin, reference.pivot_size, false);
    cout << "result data (size " << result.data.size() << "):" << endl;
    print_vector(result.data, result.pivot_begin, result.pivot_size, true);
}


bool fill_and_assert_result(test_case& result, partition some_partition, const test_case& reference) {
    iter res = some_partition(result.data.begin(), result.data.end(), result.pivot);
    result.pivot_begin = res - result.data.begin();
    result.pivot_size = 0;
    return assert_partition(false, reference, result);
}


bool fill_and_assert_result(test_case& result, range_partition some_partition, const test_case& reference) {
    iter_range res = some_partition(result.data.begin(), result.data.end(), result.pivot);
    result.pivot_begin = res.first - result.data.begin();
    result.pivot_size = res.second - res.first;
    return assert_partition(true, reference, result);
}


}
