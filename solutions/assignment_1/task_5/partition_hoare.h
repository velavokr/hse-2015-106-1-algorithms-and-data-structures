#pragma once

#include "task_5.h"

namespace task_5 {

iter partition_hoare(iter begin, iter end, int pivot);

iter_range range_partition_hoare_1pass(iter begin, iter end, int pivot);

iter_range range_partition_hoare_2pass(iter begin, iter end, int pivot);

}
