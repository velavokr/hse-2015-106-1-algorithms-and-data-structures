#include "partition_cormen.h"

#include <functional>

namespace task_5 {

template <typename condition>
iter do_partition_cormen_condition(iter begin, iter end, condition cond) {
    iter small_end = begin;

    while (true) {
        if (begin == end) {
            return small_end;
        }

        if (cond(*begin)) {
            iter_swap(small_end, begin);
            ++small_end;
        }

        ++begin;
    }
}


iter partition_cormen(iter begin, iter end, int pivot) {
    return do_partition_cormen_condition(begin, end, bind2nd(less<int>(), pivot));
}


iter_range range_partition_cormen_2pass(iter begin, iter end, int pivot) {
    begin = partition_cormen(begin, end, pivot);
    return iter_range(begin, do_partition_cormen_condition(begin, end, bind2nd(equal_to<int>(), pivot)));
}


iter_range range_partition_cormen_1pass(iter begin, iter end, int pivot) {
    iter small_end = begin;
    iter equal_begin = end;

    while (true) {
        if (begin == equal_begin) {
            break;
        }

        if (*begin < pivot) {
            iter_swap(small_end, begin);
            ++small_end;
            ++begin;
        } else if (*begin > pivot) {
            ++begin;
        } else {
            --equal_begin;
            iter_swap(equal_begin, begin);
        }
    }

    iter p_end = small_end;

    while (equal_begin != end) {
        iter_swap(p_end, equal_begin);
        ++p_end;
        ++equal_begin;
    }

    return iter_range(small_end, p_end);
}


}
