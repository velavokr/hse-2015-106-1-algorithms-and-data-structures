#pragma once

#include "task_5.h"

#include <cassert>
#include <iostream>
#include <sstream>
#include <string>

namespace task_5 {

typedef pair<int_vec, int> test_data;

struct test_variant {
    size_t size;
    size_t pivot_begin;
    size_t pivot_size;

    test_variant()
        : size()
        , pivot_begin()
        , pivot_size()
    {}

    test_variant(size_t sz, int p_begin, int p_sz)
        : size(sz)
        , pivot_begin(p_begin)
        , pivot_size(p_sz)
    {
        assert(sz >= 0);
        assert(p_begin >= 0);
        assert(p_begin <= (int)sz);
        assert(p_sz <= (int)sz - p_begin);
    }

    bool operator<(const test_variant& var) const {
        if (size < var.size)
            return true;

        if (size == var.size) {
            if (pivot_begin < var.pivot_begin) {
                return true;
            }

            if (pivot_begin == var.pivot_begin) {
                return pivot_size < var.pivot_size;
            }
        }

        return false;
    }

    bool operator==(const test_variant& var) const {
        return size == var.size && pivot_begin == var.pivot_begin && pivot_size == var.pivot_size;
    }
};

typedef vector<test_variant> test_variant_vec;


enum data_type {
    data_type_none,
    data_type_sorted,
    data_type_reverse_sorted,
    data_type_shuffled
};


struct test_case : public test_variant {
    data_type type;
    int_vec data;
    int pivot;

    test_case()
        : type(data_type_none)
        , pivot()
    {}

    test_case(const test_variant& test, data_type t)
        : test_variant(test)
        , type(t)
        , pivot()
    {}

    bool operator==(const test_case& other) const {
        return test_variant::operator ==(other) && pivot == other.pivot && data == other.data;
    }

    bool operator<(const test_case& other) const {
        if (test_variant::operator <(other)) {
            return true;
        }

        if (test_variant::operator==(other)) {
            if (pivot < other.pivot) {
                return true;
            }

            if (pivot == other.pivot) {
                return data < other.data;
            }
        }

        return false;
    }
};


typedef vector<test_case> test_case_vec;


struct test_parameters {
    size_t max_size;
    size_t neighbourhood_radius;
    size_t boundary_check_size;
    unsigned random_checks_count;
    unsigned shuffles_count;

    test_parameters()
        : max_size(64u)
        , neighbourhood_radius(2)
        , boundary_check_size(3)
        , random_checks_count(10)
        , shuffles_count(10)
    {}
};


test_variant_vec generate_test_variants(test_parameters);

test_case_vec generate_test_cases(test_parameters, const test_variant_vec&);

void print_test(const test_case& reference, const test_case& result);

bool fill_and_assert_result(test_case& result, partition some_partition, const test_case& reference);

bool fill_and_assert_result(test_case& result, range_partition some_partition, const test_case& reference);

template <typename partition_function>
bool test_partition(bool fail_fast, partition_function some_partition, const test_case_vec& tests) {
    size_t ok = 0;
    size_t failed = 0;
    for (test_case_vec::const_iterator it = tests.begin(); it != tests.end(); ++it) {
        test_case result = *it;

        if (fill_and_assert_result(result, some_partition, *it)) {
            print_test(*it, result);
            cout << "test " << ok << " passed" << endl;
            ++ok;
        } else {
            ++failed;
            print_test(*it, result);
            cout << "test " << failed << " failed" << endl;
            if (fail_fast) {
                break;
            }
        }
        cout << "----------------" << endl;
    }

    cout << ok << " tests passed" << endl;
    cout << failed << " tests failed" << endl;
    return !failed;
}


}
