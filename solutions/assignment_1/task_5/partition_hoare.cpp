#include "partition_stl.h"

#include <functional>

namespace task_5 {

template <typename condition>
iter do_partition_stl_condition(iter begin, iter end, condition cond) {
    while (true) {
        while (true) {
            if (begin == end) {
                return begin;
            }

            if (cond(*begin)) {
                ++begin;
            } else {
                break;
            }
        }

        --end;

        while (true) {
            if (begin == end) {
                return begin;
            }

            if (!cond(*end)) {
                --end;
            } else {
                break;
            }
        }

        iter_swap(begin, end);

        ++begin;
    }
}


iter partition_stl(iter begin, iter end, int pivot) {
    return do_partition_stl_condition(begin, end, bind2nd(less<int>(), pivot));
}


iter_range range_partition_stl_2pass(iter begin, iter end, int pivot) {
    begin = partition_stl(begin, end, pivot);
    return iter_range(begin, do_partition_stl_condition(begin, end, bind2nd(equal_to<int>(), pivot)));
}


void do_range_partition_stl(iter& small_end, iter& big_begin, iter& equal_end, iter& equal_begin, iter begin, int pivot) {
    while (true) {
        while (true) {
            if (big_begin == small_end) {
                return;
            }

            if (*small_end < pivot) {
                ++small_end;
            } else if (*small_end == pivot) {
                iter_swap(small_end, equal_end);
                ++equal_end;
                ++small_end;
            } else {
                break;
            }
        }

        --big_begin;

        while (true) {
            if (big_begin == small_end) {
                return;
            }

            if (*big_begin > pivot) {
                --big_begin;
            } else if (*big_begin == pivot) {
                --equal_begin;
                iter_swap(big_begin, equal_begin);
                --big_begin;
            } else {
                break;
            }
        }

        iter_swap(big_begin, small_end);
        ++small_end;
    }
}


iter_range range_partition_stl_1pass(iter begin, iter end, int pivot) {
    iter small_end = begin;
    iter equal_end = begin;
    iter big_begin = end;
    iter equal_begin = end;

    do_range_partition_stl(small_end, big_begin, equal_end, equal_begin, begin, pivot);

    while (equal_end != begin) {
        --small_end;
        --equal_end;
        iter_swap(small_end, equal_end);
    }

    while (equal_begin != end) {
        iter_swap(big_begin, equal_begin);
        ++big_begin;
        ++equal_begin;
    }

    return iter_range(small_end, big_begin);
}


}
