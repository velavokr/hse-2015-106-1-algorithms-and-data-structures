#pragma once

#include "task_5.h"

namespace task_5 {

// std partition wrapper with pivoting point (pivot_iter)
iter partition_std(iter begin, iter end, int pivot);

// std partition with pivoting range (pivot_begin, pivot_end)
iter_range range_partition_std(iter begin, iter end, int pivot);

}
