#include "partition_std.h"

#include <algorithm>
#include <functional>

namespace task_5 {

iter partition_std(iter begin, iter end, int pivot) {
    return std::partition(begin, end, bind2nd(less<int>(), pivot));
}


iter_range range_partition_std(iter begin, iter end, int pivot) {
    begin = partition_std(begin, end, pivot);
    return iter_range(begin, std::partition(begin, end, bind2nd(equal_to<int>(), pivot)));
}

}
