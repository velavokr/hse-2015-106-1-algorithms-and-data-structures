#include "partition_cormen.h"
#include "partition_hoare.h"
#include "partition_std.h"

#include "task_5_tests.h"

int main() {
    using namespace task_5;
    test_parameters params;
    test_variant_vec variants = generate_test_variants(params);

    std::cout << variants.size() << " test variants to go" << std::endl;

    test_case_vec tests = generate_test_cases(params, variants);

    std::cout << tests.size() << " test cases to go" << std::endl << std::endl;

    std::cout << "2-pass hoare range partition" << std::endl;

    if (!test_partition(true, range_partition_hoare_2pass, tests)) {
        std::cout << "2-pass hoare range partition failed some tests" << std::endl;
        return 0;
    } else {
        std::cout << "all tests passed" << std::endl;
    }

    std::cout << "##############" << std::endl << std::endl;

    std::cout << "1-pass hoare range partition" << std::endl;

    if (!test_partition(true, range_partition_hoare_1pass, tests)) {
        std::cout << "1-pass hoare range partition failed some tests" << std::endl;
        return 0;
    } else {
        std::cout << "all tests passed" << std::endl;
    }

    std::cout << "##############" << std::endl << std::endl;

    std::cout << "2-pass 'cormen' range partition" << std::endl;

    if (!test_partition(true, range_partition_cormen_2pass, tests)) {
        std::cout << "2-pass cormen range partition failed some tests" << std::endl;
        return 0;
    } else {
        std::cout << "all tests passed" << std::endl;
    }

    std::cout << "##############" << std::endl << std::endl;

    std::cout << "1-pass 'cormen' range partition" << std::endl;

    if (!test_partition(true, range_partition_cormen_1pass, tests)) {
        std::cout << "1-pass cormen range partition failed some tests" << std::endl;
        return 0;
    } else {
        std::cout << "all tests passed" << std::endl;
    }

    return 0;
}
