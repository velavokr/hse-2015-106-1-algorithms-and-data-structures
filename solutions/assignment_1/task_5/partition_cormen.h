#pragma once

#include "task_5.h"

namespace task_5 {

// cormen style with pivoting point (pivot_iter)

iter partition_cormen(iter begin, iter end, int pivot);

iter_range range_partition_cormen_1pass(iter begin, iter end, int pivot);

iter_range range_partition_cormen_2pass(iter begin, iter end, int pivot);

}
